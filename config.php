<?php

// Moodle configuration file

unset($CFG);
global $CFG;
$CFG = new stdClass();

$CFG->dbtype = 'mysqli';
$CFG->dblibrary = 'native';
$CFG->dbhost = '58.187.9.254';
$CFG->dbname = 'moodle_test4';
$CFG->dbuser = 'dev123';
$CFG->dbpass = 'dbmoodle_test4_notfound';
$CFG->prefix = 'top_';
$CFG->dboptions = array(
    'dbpersist' => 0,
    'dbport' => '',
    'dbsocket' => '',
    'dbcollation' => 'utf8mb4_unicode_ci',
);

$CFG->wwwroot = 'https://nativetestdev.topicanative.edu.vn';
$CFG->dataroot = '/var/www/html/lienbtt/moodledata';
$CFG->admin = 'admin';

$CFG->directorypermissions = 0777;
$CFG->disableupdatenotifications = true;
$API = array(
    'api' => 'https://testenglish.topicanative.edu.vn:12345/api/',
    'getMarkMonth' => 'englishtest/getMarkMonth',
    'setRating' => 'englishtest/setRating',
    'checkRating' => 'englishtest/getRating',
    'getMarkWeek' => 'englishtest/getMarkWeek',
    'checkStatusTopicaLog' => 'englishtest/checkStatusTopicaLog',
    'updateTopicaLog' => 'englishtest/updateTopicaCheckLog',
    'setTopicaLog' => 'englishtest/setTopicaCheckLog'
);


$CFG->config_api_advisor_real = array(
    'server' => 'http://advisor.topicanative.edu.vn/',
    'http_auth' => 'basic',
    'api_name' => 'key',
    'http_user' => 'nativetest_v2',
    'http_pass' => 'nativetest',
    'api_key' => '4rMyqGsrESXxt5ivvyuht12062016nativetest'
);

$CFG->config_api_advisor_vip = array(
    'server' => 'http://advisorvip.topicanative.edu.vn/',
    'http_auth' => 'basic',
    'api_name' => 'key',
    'http_user' => 'nativetestvip_v2',
    'http_pass' => 'nativetestvip',
    'api_key' => '4rMyqGsrESXxt5ivvyuht12062016nativetest'
);


require_once(__DIR__ . '/lib/setup.php');

// // There is no php closing tag in this file,
// // it is intentional because it prevents trailing whitespace problems!
