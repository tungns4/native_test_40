<?php
/**
 * Created by PhpStorm.
 * User: ishineguy
 * Date: 2018/06/16
 * Time: 19:31
 */

namespace mod_readaloud;

defined('MOODLE_INTERNAL') || die();

class constants
{
//component name, db tables, things that define app
const M_COMPONENT='mod_readaloud';
const M_FILEAREA_SUBMISSIONS='submission';
const M_TABLE='readaloud';
const M_USERTABLE='readaloud_attempt';
const M_AITABLE='readaloud_ai_result';
const M_MODNAME='readaloud';
const M_URL='/mod/readaloud';
const M_CLASS='mod_readaloud';

//grading options
const M_GRADEHIGHEST= 0;
const M_GRADELOWEST= 1;
const M_GRADELATEST= 2;
const M_GRADEAVERAGE= 3;
const M_GRADENONE= 4;
//accuracy adjustment method options
const ACCMETHOD_NONE =0;
const ACCMETHOD_AUTO =1;
const ACCMETHOD_FIXED =2;
//whot to display to user when reviewing activity options
const POSTATTEMPT_NONE=0;
const POSTATTEMPT_EVAL=1;
const POSTATTEMPT_EVALERRORS=2;
//more review mode options
const REVIEWMODE_NONE=0;
const REVIEWMODE_MACHINE=1;
const REVIEWMODE_HUMAN=2;
const REVIEWMODE_SCORESONLY=3;
//to use or not use machine grades
const MACHINEGRADE_NONE=0;
const MACHINEGRADE_MACHINE=1;

//CSS ids/classes
const M_RECORD_BUTTON='mod_readaloud_record_button';
const M_START_BUTTON='mod_readaloud_start_button';
const M_UPDATE_CONTROL='mod_readaloud_update_control';
const M_DRAFT_CONTROL='mod_readaloud_draft_control';
const M_PROGRESS_CONTAINER='mod_readaloud_progress_cont';
const M_HIDER='mod_readaloud_hider';
const M_STOP_BUTTON='mod_readaloud_stop_button';
const M_WHERETONEXT_CONTAINER='mod_readaloud_wheretonext_cont';
const M_RECORD_BUTTON_CONTAINER='mod_readaloud_record_button_cont';
const M_START_BUTTON_CONTAINER='mod_readaloud_start_button_cont';
const M_STOP_BUTTON_CONTAINER='mod_readaloud_stop_button_cont';
const M_RECORDERID='therecorderid';
const M_RECORDING_CONTAINER='mod_readaloud_recording_cont';
const M_RECORDER_CONTAINER='mod_readaloud_recorder_cont';
const M_DUMMY_RECORDER='mod_readaloud_dummy_recorder';
const M_RECORDER_INSTRUCTIONS_RIGHT='mod_readaloud_recorder_instr_right';
const M_RECORDER_INSTRUCTIONS_LEFT='mod_readaloud_recorder_instr_left';
const M_INSTRUCTIONS_CONTAINER='mod_readaloud_instructions_cont';
const M_PASSAGE_CONTAINER='mod_readaloud_passage_cont';
const M_POSTATTEMPT= 'mod_readaloud_postattempt';
const M_FEEDBACK_CONTAINER='mod_readaloud_feedback_cont';
const M_ERROR_CONTAINER='mod_readaloud_error_cont';
const M_GRADING_ERROR_CONTAINER='mod_readaloud_grading_error_cont';
const M_GRADING_ERROR_IMG='mod_readaloud_grading_error_img';
const M_GRADING_ERROR_SCORE='mod_readaloud_grading_error_score';
const M_GRADING_WPM_CONTAINER='mod_readaloud_grading_wpm_cont';
const M_GRADING_WPM_IMG='mod_readaloud_grading_wpm_img';
const M_GRADING_WPM_SCORE='mod_readaloud_grading_wpm_score';
const M_GRADING_ACCURACY_CONTAINER='mod_readaloud_grading_accuracy_cont';
const M_GRADING_ACCURACY_IMG='mod_readaloud_grading_accuracy_img';
const M_GRADING_ACCURACY_SCORE='mod_readaloud_grading_accuracy_score';
const M_GRADING_SESSION_SCORE='mod_readaloud_grading_session_score';
const M_GRADING_SESSIONSCORE_CONTAINER='mod_readaloud_grading_sessionscore_cont';
const M_GRADING_SCORE='mod_readaloud_grading_score';
const M_GRADING_PLAYER_CONTAINER='mod_readaloud_grading_player_cont';
const M_GRADING_PLAYER='mod_readaloud_grading_player';
const M_GRADING_ACTION_CONTAINER='mod_readaloud_grading_action_cont';
const M_GRADING_FORM_SESSIONTIME='mod_readaloud_grading_form_sessiontime';
const M_GRADING_FORM_SESSIONSCORE='mod_readaloud_grading_form_sessionscore';
const M_GRADING_FORM_WPM='mod_readaloud_grading_form_wpm';
const M_GRADING_FORM_ACCURACY='mod_readaloud_grading_form_accuracy';
const M_GRADING_FORM_SESSIONENDWORD='mod_readaloud_grading_form_sessionendword';
const M_GRADING_FORM_SESSIONERRORS='mod_readaloud_grading_form_sessionerrors';
const M_GRADESADMIN_CONTAINER='mod_readaloud_gradesadmin_cont';
const M_HIDDEN_PLAYER='mod_readaloud_hidden_player';
const M_HIDDEN_PLAYER_BUTTON='mod_readaloud_hidden_player_button';
const M_HIDDEN_PLAYER_BUTTON_ACTIVE='mod_readaloud_hidden_player_button_active';
const M_HIDDEN_PLAYER_BUTTON_PAUSED='mod_readaloud_hidden_player_button_paused';
const M_HIDDEN_PLAYER_BUTTON_PLAYING='mod_readaloud_hidden_player_button_playing';
const M_EVALUATED_MESSAGE='mod_readaloud_evaluated_message';
}