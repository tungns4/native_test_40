<?php
require_once('../../../config.php');

?>

<html>
<head>
    <script
            src="http://code.jquery.com/jquery-1.12.4.js"
            integrity="sha256-Qw82+bXyGq6MydymqBxNPYTaUXXq7c8v3CwiYwLLNXU="
            crossorigin="anonymous"></script>
    <!--    <link rel="stylesheet" type="text/javascript" href="js/jquery-1.12.4.js?v=1">-->
    <script rel="stylesheet" type="text/javascript" src="js/bootstrap/jquery.dataTables.min.js"></script>
    <script rel="stylesheet" type="text/javascript" src="js/bootstrap/bootstrap.min.js"></script>
    <script rel="stylesheet" type="text/javascript" src="js/bootstrap/dataTables.bootstrap4.min.js"></script>
    <script rel="stylesheet" type="text/javascript" src="js/bootstrap/dataTables.buttons.min.js"></script>
    <link rel="stylesheet" type="text/javascript" href="js/bootstrap/jszip.min.js"/>
    <link rel="stylesheet" type="text/javascript" href="js/bootstrap/buttons.html5.min.js"/>
    <link rel="stylesheet" type="text/javascript" href="js/bootstrap/buttons.colVis.min.js"/>
    <link rel="stylesheet" type="text/javascript" href="js/bootstrap/buttons.bootstrap4.min.js"/>
    <link rel="stylesheet" type="text/javascript" href="js/bootstrap/bootstrap-select.min.js"/>
    <link rel="stylesheet" type="text/javascript" href="js/pdfmake.min.js"/>
    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap-select.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/bootstrap/daterangepicker.css"/>
    <link rel="stylesheet" type="text/css" href="css/bootstrap/dataTables.bootstrap4.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/bootstrap/buttons.bootstrap4.min.css"/>
    <script type="javascript">
        function load_params() {
            var table_height = $(window).height() - (420 - ($(window).height() * 0.02));
            document.getElementById("report_table").innerHTML = '<table class="table display nowrap" id="show_data" style="overflow: scroll; max-height: ' + table_height + 'px; width:100%; display:block;"><thead><tr id="tb_head"></tr></thead><tbody id="tb_body"></tbody></table>';

            var tb_head = '<tr><th>username</th>';
            tb_head += '<th>vocabulary</th>';
            tb_head += '<th>conservation</th>';
            tb_head += '<th>listening</th>';
            tb_head += '<th>dictation</th>';
            tb_head += '<th>total</th>';
            tb_head += '<th>state</th>';
            tb_head += '<th>start</th>';
            tb_head += '<th>finish</th>';
            tb_head += '<th>post_crm</th></tr>';
            document.getElementById("tb_head").innerHTML = tb_head;
            $.ajax({
                method: "POST",
                url: '../../../public/libs/api.php',
                data: {
                    action: 'api',
                    url: 'http://103.56.158.233:1000/api/englisttest/get_marks_by_time',
                    from_date: document.getElementById('from_date').value,
                    to_date: document.getElementById('to_date').value,
                    username: document.getElementById('username').value
                }
            }).success(function (response) {
                if (response) {
                    var res = JSON.parse(response);
                    var tb_body = '';
                    res['data'].forEach(function (item) {
                        var masks = JSON.parse(item['marks']);
                        var voca = parseFloat(masks['VOCABULARY']).toFixed(2);
                        var conver = parseFloat(masks['CONVERSATIONAL_EXPRESSION']).toFixed(2);
                        var listen = parseFloat(masks['LISTENING']).toFixed(2);
                        var dict = parseFloat(masks['DICTATION']).toFixed(2);
                        var gram = parseFloat(masks['GRAMMAR']).toFixed(2);
                        var read = parseFloat(masks['READING']).toFixed(2);
                        var total = voca + conver + listen + dict + gram + read;
                        tb_body += '<tr>';
                        tb_body += '<td>' + item['username'] + '</td>';
                        tb_body += '<td>' + voca + '</td>';
                        tb_body += '<td>' + conver + '</td>';
                        tb_body += '<td>' + listen + '</td>';
                        tb_body += '<td>' + dict + '</td>';
                        tb_body += '<td>' + gram + '</td>';
                        tb_body += '<td>' + read + '</td>';
                        tb_body += '<td>' + total + '</td>';
                        tb_body += '<td>' + item['state'] + '</td>';
                        tb_body += '<td>' + item['timestart'] + '</td>';
                        tb_body += '<td>' + item['timefinish'] + '</td>';
                        tb_body += '<td>' + item['post_crm'] + '</td>';
                        tb_body += '</tr>';
                    });
                    document.getElementById("tb_body").innerHTML = tb_body;
                    var table = $('#show_data').DataTable({
                        lengthChange: true,
                        autoWidth: true
                       buttons: ['excel']
                    });
                    //add nút export vào datatable
                    table.buttons().container().appendTo('#show_data_wrapper .col-md-6:eq(0)');
                }
            });

        }

        function excel_report(tableID) {
            var tab_text = "<table border='2px'><tr bgcolor='#dbac69'>";
            var textRange;
            var j = 0;
            tab = document.getElementById(tableID); // id of table

            for (j = 0; j < tab.rows.length; j++) {
                tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
                //tab_text=tab_text+"</tr>";
            }

            tab_text = tab_text + "</table>";
            tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
            tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
            tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");

            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
            {
                txtArea1.document.open("txt/html", "replace");
                txtArea1.document.write(tab_text);
                txtArea1.document.close();
                txtArea1.focus();
                sa = txtArea1.document.execCommand("SaveAs", true, "Say Thanks to Sumit.xls");
            }
            else                 //other browser not tested on IE 11
                sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

            return (sa);
        }
    </script>

    <style>
        #show_data table, #show_data tr{
            color: #dbac69;
            background: #fff;
        }
        #show_data table, #show_data th{
            background: #dbac69;
            color: #000;
        }
        #show_data tbody {
            height: 80em;
            overflow: scroll;
        }

        #container {
            width: auto;
            font-size: 11px;
        }

        #page {
            position: absolute !important;
            width: 100% !important;
            height: 100% !important;
            top: 0 !important;
            left: 0 !important;
            overflow: auto !important;
            background-size: 100% 100% !important;
            background: #424242;
            color: #dbac69;
        }

        #header {
            height: 40px;
            background-color: rgba(0, 0, 0, 0.8);
            width: 100%;
            position: relative;

        }

        #header .img-logo {
            margin-left: 5%;
        }

        .content span {
            font-size: 16px !important;
        }

        #wrapper_container {
            margin-bottom: 0;
        }

        #welcome {
            width: 50%;
            padding: 10px;
            float: none;
            margin: 0 auto;
            margin-top: 130px;
            font-size: 13px;
            border-bottom: 0;
            box-sizing: border-box;
            background-color: rgba(0, 0, 0, 0.9);
            color: #dbac69;
        }

        #notification {
            margin-top: 0;
            background-color: rgba(0, 0, 0, 0.5) !important;
            color: #dbac69;
            width: 50%;
            margin: 0 auto;
        }

        #notification .note {
            color: #dbac69;
        }

        #footer {
            border-top: 3px solid #dbac69;
            height: 80px;
            background-color: #111;
            width: 100%;

            bottom: 0;
            position: fixed;
            left: 0;
        }

        #footer .gt-topica {
            width: 43%;
            float: left;
            height: 80px;
            color: #dbac69;
            font-size: 13px;
        }

        #footer .content-footer {
            width: 80%;
            height: 80px;
            margin: 0 auto;
            float: none;
        }

        #footer .logo-bottom {
            width: 15%;
            height: 80px;
            box-sizing: border-box;
            padding: 10px;
            float: left;
        }

        #footer .logo-bottom img {
            width: 100%;
            height: auto;
        }

        #footer .contact-footer {
            width: 25%;
            height: 80px;
            float: left;
            color: #dbac69;
        }

        #footer .link-footer {
            width: 10%;
            height: 80px;
            float: left;
            color: #dbac69;
        }

        #footer .link-footer p a {
            color: #dbac69;
            text-decoration: none;
        }

        #footer .contact-footer .icon-contact {
            width: 20%;
            height: 80px;
            float: left;
        }

        #footer .contact-footer .icon-contact i {
            float: right;
            margin-top: 10px;
            margin-right: 5px;
        }

        #footer .contact-footer .list-contact {
            width: 80%;
            margin-top: 10px;
            font-size: 12px;
            height: 70px;
            float: left;

        }
        .form-group{
            margin-left: 20px;
        }

    </style>


</head>

<body>
<div id="page">
    <div id="header">
        <div id="container">
            <div class="header_logo">
                <a class="">
                    <img src='http://45.124.94.222/theme/boost/img/logo.png' style="height: 35px;" alt=''
                         class='img-logo'/>
                </a>

            </div>
        </div>
    </div>

    <br/>
    <form class="form-inline" method="post">
        <div class="form-group">
            <label for="email">From date:</label>
            <input type="date" class="form-control" id="from_date">
        </div>
        <div class="form-group">
            <label for="email">To date:</label>
            <input type="date" class="form-control" id="to_date">
        </div>

        <div class="form-group">
            <label for="email">Username:</label>
            <input type="email" class="form-control" id="username">
        </div>

        <iframe id="txtArea1" style="display:none"></iframe>
        <button type="button" class="btn btn-success" onclick="load_params()" style="margin-left: 30px;">Lọc</button>
        <button type="button" class="btn btn-secondary buttons-excel buttons-html5" onclick="excel_report('show_data')" style="margin-left: 10px;">
            Export XLS
        </button>
    </form>


    <div class="panel-body" id="report_table">

    </div>


    <!--footer -->
    <div id='footer'>
        <div class='content-footer'>
            <div class='logo-bottom'>
                <img src='http://englishtest.topicanative.edu.vn/theme/boost/img/logo.png' style="width: 100%;" alt='' class='img-logo'/>
            </div>
            <div class='gt-topica'>
                <ul>
                    <p>
                    <li>TOPICA Là đối tác đầu tiên của tập đoàn Voxy (Hoa Kỳ) tại Việt Nam</li>
                    <li>45000+ học viên đã theo học tại TOPICA Native</li>
                    <li>83,7% học viên tăng 200+ điểm nói sau một khóa học</li>
                </ul>
            </div>
            <div class='contact-footer'>
                <div class='icon-contact'>

                </div>
                <div class='list-contact'>
                    Vietnam: 1800 6885 (nhánh 3)<br>
                    Thailand: +6621054246 (กด 1)<br>
                    Indonesia: +622129223039
                </div>
            </div>

            <div class='link-footer'>
                <p><a href='https://www.youtube.com/user/TOPMITO'><i class='fa fa-youtube-play'
                                                                     aria-hidden='true'></i>
                        Youtube</a><br>
                    <a href='https://www.facebook.com/TOPMITO.edu.vn/?fref=ts'><i class='fa fa-facebook'
                                                                                  aria-hidden='true'></i>&nbsp;Facebook</a><br>
                </p>
            </div>
        </div>
    </div>
</div>


</body>
</html>







