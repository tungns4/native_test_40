<?php
    require_once '../../config.php';
    global $DB;

    $obj = new stdClass();
    $obj->user_name         = $_POST["user_name"];
    $obj->num_star          = $_POST["num_star"];
    $obj->evaluate1         = $_POST["evaluate1"];
    $obj->evaluate2         = $_POST["evaluate2"];
    $obj->note              = $_POST["note"];
//    $obj->attemptid = $_POST["attemptid"];
//    $obj->type_attempt = $_POST["type_attempt"];
    try{
        $rs = $DB->insert_record("topica_rating", $obj);
        echo $rs;
    }catch (Exception $e) {
        echo 0;
    }
?>
