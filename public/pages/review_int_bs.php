<?php
require_once 'dompdf/lib/html5lib/Parser.php';
require_once 'dompdf/lib/php-font-lib/src/FontLib/Autoloader.php';
require_once 'dompdf/lib/php-svg-lib/src/autoload.php';
require_once 'dompdf/src/Autoloader.php';
Dompdf\Autoloader::register();

use Dompdf\Dompdf;

$dompdf = new Dompdf();

require_once '../../config.php';
require_once '../libs/api.php';

$attemptid = required_param('attempt', PARAM_INT);
$link_home = $CFG->wwwroot . '/my/?lang=vi';
$link_logout = $CFG->wwwroot . '/login/logout.php';
$link_view = $CFG->wwwroot . '/mod/quiz/review.php?attempt=' . $attemptid . '&view=1';
$link_detail = 'summary.php?attempt=' . $attemptid;
$type_attempt = 'bs_int_month';
$request_data = array(
    'attemptid' => $attemptid
);
$response = call_API($API['api'] . $API['getMarkMonth'], $request_data);
$check_rating = call_API($API['api'] . $API['checkRating'], $request_data);
$bool_check_rating = json_encode($check_rating['success']);

if (boolval($response['success'])) {
    $api_data = $response['data'];
    $topicaMark = $response['data']->marks;
    $topicaMark = json_decode($topicaMark);
    $voca = ceil($topicaMark->VOCABULARY);
    $conver = ceil($topicaMark->CONVERSATIONAL_EXPRESSION);
    $listen = ceil($topicaMark->LISTENING);
    $dict = ceil($topicaMark->DICTATION);
    $total = $voca + $conver + $listen + $dict;
    $name = $api_data->firstname . ' ' . $api_data->lastname;
    $username = $api_data->username;
    $date = $api_data->created_at;
    $url_api = $API['api'] . $API['setRating'];
}

if (isset($_POST['pdf']) && intval($_POST['pdf']) > 0) {
    $date = $api_data->created_at;
    $html = file_get_contents('pdf.html');
    $html = str_replace('pvt_name', $name, $html);
    $html = str_replace('pvt_username', $username, $html);
    $html = str_replace('pvt_date', $date, $html);
    $html = str_replace('pvt_vocab', $voca, $html);
    $html = str_replace('pvt_conver', $conver, $html);
    $html = str_replace('pvt_listen', $listen, $html);
    $html = str_replace('pvt_dict', $dict, $html);
    $html = str_replace('pvt_total', $total, $html);
    $html = mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8');
    $dompdf->loadHtml($html);
    $dompdf->setPaper('A4', 'landscape');
    $dompdf->render();
    $dompdf->stream($username . '.pdf');
}
?>
<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta charset="utf-8">
    <link rel="shortcut icon"
          href="http://tracnghiem.topicanative.edu.vn/theme/image.php/clean/theme/1468830243/favicon"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/rating.css">
    <style>
        #container {
            width: auto;
        }

        #page {
            background-image: linear-gradient(rgba(255, 255, 255, 0.5), rgba(255, 255, 255, 0.5)), url(img/brgtn.jpeg) !important;
            position: absolute !important;
            width: 100% !important;
            height: 100% !important;
            top: 0 !important;
            left: 0 !important;
            overflow: auto !important;
            background-size: 100% 100% !important;
        }

        #hedder-tn {
            height: 50px;
            background-color: rgba(0, 0, 0, 0.8);
            width: 100%;

            position: absolute;

        }

        #hedder-tn .img-logo {
            height: 50px;
            box-sizing: border-box;
            padding: 10px;
            margin-left: 10%;
        }

        .content span {
            font-size: 16px !important;
        }

        #wrapper_container {
            margin-bottom: 0;
        }

        #welcome {
            width: 50%;
            padding: 10px;
            float: none;
            margin: 0 auto;
            margin-top: 130px;
            font-size: 13px;
            border-bottom: 0;
            box-sizing: border-box;
            background-color: rgba(0, 0, 0, 0.9);
            color: #dbac69;
        }

        #notification {
            margin-top: 0;
            background-color: rgba(0, 0, 0, 0.5) !important;
            color: #dbac69;
            width: 50%;
            margin: 0 auto;
        }

        #notification .note {
            color: #dbac69;
        }

        #footer-tn {
            border-top: 3px solid #dbac69;
            height: 80px;
            background-color: #111;
            width: 100%;

            bottom: 0;
            position: fixed;
            left: 0;
        }

        #footer-tn .gt-topica {
            width: 43%;
            float: left;
            height: 80px;
            color: #dbac69;
            font-size: 13px;
        }

        #footer-tn .content-footer {
            width: 80%;
            height: 80px;
            margin: 0 auto;
            float: none;
        }

        #footer-tn .logo-bottom {
            width: 15%;
            height: 80px;
            box-sizing: border-box;
            padding: 10px;
            float: left;
        }

        #footer-tn .logo-bottom img {
            width: 100%;
            height: auto;
        }

        #footer-tn .contact-footer {
            width: 25%;
            height: 80px;
            float: left;
            color: #dbac69;
        }

        #footer-tn .address-bottom {
            width: 15%;
            height: 80px;
            float: left;
            color: #dbac69;
        }

        #footer-tn .link-footer {
            width: 10%;
            height: 80px;
            float: left;
            color: #dbac69;
        }

        #footer-tn .link-footer p a {
            color: #dbac69;
            text-decoration: none;
        }

        #footer-tn .contact-footer .icon-contact {
            width: 20%;
            height: 80px;
            float: left;
        }

        #footer-tn .contact-footer .icon-contact i {
            float: right;
            margin-top: 10px;
            margin-right: 5px;
        }

        #footer-tn .contact-footer .list-contact {
            width: 80%;
            margin-top: 10px;
            font-size: 12px;
            height: 70px;
            float: left;

        }

        #wrapper_footer {
            display: none !important;
        }

        #wrapper_header {
            display: none !important;
        }
    </style>
</head>
<body>
<div id="page">
    <div id="wrapper_container">
        <div id="container">
            <div class="content">
                <span id="maincontent"></span>
                <link rel='stylesheet' href='http://fontawesome.io/assets/font-awesome/css/font-awesome.css'>
                <?php
                if (boolval($response['success'])) {
                    $api_data = $response['data'];
                    $topicaMark = $response['data']->marks;
                    $topicaMark = json_decode($topicaMark);
                    $voca = ceil($topicaMark->VOCABULARY);
                    $conver = ceil($topicaMark->CONVERSATIONAL_EXPRESSION);
                    $listen = ceil($topicaMark->LISTENING);
                    $dict = ceil($topicaMark->DICTATION);
                    $total = $voca + $conver + $listen + $dict;
                    $name = $api_data->firstname . ' ' . $api_data->lastname;
                    $username = $api_data->username;
                    $date = $api_data->created_at;
                    $url_api = $API['api'] . $API['setRating'];
                ?>
                <button type="button" class="btn btn-primary btn-lg" id="btn-show-md" data-toggle="modal"
                        data-target="#myModal_rating" style="display: none"></button>
                <script>
                    $(document).ready(function () {
                        var check_rating = "<?php echo $bool_check_rating; ?>";
                        if (!check_rating || check_rating !== 'true') {
                            $("#btn-show-md").click();
                        }
                    });
                </script>
                <!-- Modal -->
                <div class="modal fade" id="myModal_rating" tabindex="-1" role="dialog" aria-hidden="false"
                     data-keyboard="false">
                    <div class="modal-dialog" style="width: 502px;" role="document">
                        <div class="modal-content" style="border-radius: 0px;">
                            <div class="modal-body" style="padding: 0;">

                                <div class="container show-rating">
                                    <div id="title-star">
                                        <h4><i style="font-size: 14px; color: #fff;">Cảm ơn bạn đã hoàn thành bài kiểm
                                                tra. Nhằm nâng cao chất lượng <br> chương trình học, chúng tôi mong muốn
                                                nhận được ý kiến đánh giá của bạn</i></h4>
                                        <h4><b>Mức độ hài lòng của bạn đối với hệ thống Native Test?</b></h4>
                                    </div>
                                    <!-- <p class="criteria" >Bình thường</p> -->
                                    <div id="start-rating">
                                        <div class="row">
                                            <b style="font-size:18px;"><span
                                                        style="margin-top: 20px; display: inline-block;"
                                                        class="status-star1">Chưa chọn</span></b>
                                            <b style="font-size:18px;"><span style="display: none; margin-top: 20px;"
                                                                             class="status-star2"> chọn</span></b>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2 col-xs-2"></div>
                                            <div class="col-md-8 rating col-xs-8">
                                                <input type="radio" id="star5" name="rating" value="5"/>
                                                <label class="full" for="star5" data-star="5"
                                                       title="Tôi rất hài lòng với hệ thống Native test này - 5 stars"></label>
                                                <input type="radio" id="star4" name="rating" value="4"/>
                                                <label class="full" for="star4" data-star="4"
                                                       title="Tôi thích hệ thống Native test này - 4 stars"></label>
                                                <input type="radio" id="star3" name="rating" value="3"/>
                                                <label class="full" for="star3" data-star="3"
                                                       title="Hệ thống Native test này chấp nhận được - 3 stars"></label>
                                                <input type="radio" id="star2" name="rating" value="2"/>
                                                <label class="full" for="star2" data-star="2"
                                                       title="Hệ thống Native test này không theo kỳ vọng của tôi - 2 stars"></label>
                                                <input type="radio" id="star1" name="rating" value="1"/>
                                                <label class="full" for="star1" data-star="1"
                                                       title="Hệ thống Native test này quá tệ - 1 star"></label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <i style="font-size: 12px;">(Chọn 1 biểu tượng để đánh giá)</i>
                                        </div>
                                        <div class="al_star">
                                            <div class="tks-gv">
                                                <p class="dgv-p">Native test cần cải thiện:</p>
                                                <div class="row dr">
                                                    <div class="item-tks1 col-xs-4 col-md-4 tks-itt" data-index="1">
                                                        <div>
                                                            <img class="img-sating" src="images/reading.png" alt="">
                                                        </div>
                                                        <div class="knd">
                                                            Nội dung bài thi
                                                        </div>
                                                        <span class="tooltiptext">
                                                                        - Bài thi có liên quan đến chương trình học trên lớp không? </br>
                                                            - Bài thi có phân tầng câu hỏi từ dễ đến khó không? </br>
                                                            - Bài thi có tạo được cảm giác thích thú khi làm bài cho HV không? </br>
                                                                    </span>
                                                    </div>
                                                    <div class="item-tks2 col-xs-4 col-md-4 tks-itt" data-index="2">
                                                        <div>
                                                            <img class="img-sating" src="images/setting.png" alt="">
                                                        </div>
                                                        <div class="knd">
                                                            Chất lượng kĩ thuật
                                                        </div>
                                                        <span class="tooltiptext">
                                                                        - HV có gặp lỗi khi truy cập để làm bài không? </br>
                                                            - HV có gặp lỗi kỹ thuật trong quá trình làm bài không? (Không gặp lỗi; Không hiển thị đáp án; Lỗi audio; Đồng hồ đếm ngược lỗi; Lỗi khác) </br>
                                                                    </span>
                                                    </div>
                                                    <div class="item-tks3 col-xs-4 col-md-4 tks-itt" data-index="3">
                                                        <div>
                                                            <img class="img-sating" src="images/television.png" alt="">
                                                        </div>
                                                        <div class="knd">
                                                            Khác
                                                        </div>
                                                        <span class="tooltiptext">
                                                                        - Màu sắc bài thi có bắt mắt không? </br>
                                                            - Hình ảnh minh họa cho các câu hỏi có thu hút không? </br>
                                                            - Các phím chức năng: biểu tượng thời gian, audio có dễ nhận biết để sử dụng không? </br>
                                                                    </span>
                                                    </div>
                                                </div>
                                                <textarea rows="2" cols="20" placeholder="Ý kiến đóng góp..."
                                                          name="y_kien" id="y_kien" class="text-dv" style=""
                                                          maxlength="1000"></textarea>
                                            </div>
                                            <div class="st_good">
                                                <h4><b>Trong thang điểm từ 1 đến 10, khả năng bạn sẵn lòng giới thiệu
                                                        người khác trải nghiệm chương trình </br> tiếng Anh Topica
                                                        Native là bao nhiêu?</b></h4>
                                                <div class="row click_dg">
                                                    <button class="btn btn16" value="1">1</button>
                                                    <button class="btn btn16" value="2">2</button>
                                                    <button class="btn btn16" value="3">3</button>
                                                    <button class="btn btn16" value="4">4</button>
                                                    <button class="btn btn16" value="5">5</button>
                                                    <button class="btn btn16" value="6">6</button>
                                                    <button class="btn btn78" value="7">7</button>
                                                    <button class="btn btn78" value="8">8</button>
                                                    <button class="btn btn910" value="9">9</button>
                                                    <button class="btn btn910" value="10">10</button>
                                                </div>
                                                <div class="row note_dg">
                                                    <div class="col-md-4 label_note1">
                                                        <label></label>
                                                        Không muốn
                                                    </div>
                                                    <div class="col-md-4 label_note2">
                                                        <label></label>
                                                        Bình thường
                                                    </div>
                                                    <div class="col-md-4 label_note3">
                                                        <label></label>
                                                        Rất sẵn lòng
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <button class="btn danh_gia submit-star"
                                                        data-username="<?php echo $username; ?>"
                                                        data-api="<?php echo $url_api; ?>"
                                                        data-attempt="<?php echo $attemptid; ?>"
                                                        data-type-attempt="<?php echo $type_attempt; ?>">Gửi đánh giá
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <button type="button" class="btn btn-default close" data-dismiss="modal"
                                    style="display: none;">Close
                            </button>
                        </div>
                    </div>
                </div>


                <div id="container" style="width:800px; margin: 0 auto;margin-top:80px;"><br><br>
                    <div style="text-align:center;">
                        <h1 style="color: #dbac69;font-weight: bolder;padding: 10px;background: rgba(0,0,0,0.8);box-sizing: border-box;text-align: center;margin-bottom:0;">
                            BÁO CÁO ĐIỂM</h1>
                    </div>
                    <div class="content-body"
                         style="overflow:auto;padding: 20px;box-sizing: border-box;margin-bottom:90px;;background: rgba(0,0,0,0.5);">

                        <div style="text-align:center"><p style="color:#fff;">(Nội dung phản hồi về bài kiểm tra)</p>
                        </div>
                        <table style="width:100%; border-collapse: collapse;">
                            <tbody>
                            <tr>
                                <th bgcolor="#dbac69"
                                    style="color:#000;border: 1px solid black;width:23%; text-align: center"
                                    height="30">Họ và tên
                                </th>
                                <th colspan="3"
                                    style="background-color:#fff;color:#000;border: 1px solid black;text-align:left;padding-left:10px;">
                                    <?php echo $name; ?>
                                </th>
                            </tr>
                            <tr>
                                <td bgcolor="#dbac69"
                                    style="color:#000;border: 1px solid black;width:23%;text-align:center;" height="30">
                                    Username
                                </td>
                                <td style="background-color:#fff;border: 1px solid black;width:23%;padding-left:10px;">
                                    <?php echo $username; ?>
                                </td>
                                <td bgcolor="#dbac69"
                                    style="color:#000;border: 1px solid black;width:23%;text-align:center;">Ngày kiểm
                                    tra
                                </td>
                                <td style="background-color:#fff;border: 1px solid black;width:23%;padding-left:10px;">
                                    <?php echo $date; ?>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <br><br>
                        <table style="width:100%; border-collapse: collapse;text-align:center;">
                            <tbody>
                            <tr>
                                <th>Điểm</th>
                                <th colspan="4" style="color:#fff">Hãy kiểm tra điểm bài thi của bạn
                                </th>
                            </tr>
                            <tr>
                                <td bgcolor="#dbac69" style="color:#000;border: 1px solid black;width:15%;">PHẦN</td>
                                <td bgcolor="#dbac69" style="color:#000;border: 1px solid black;width:15%;">Phần 1 <br>Conversational
                                    <br>Expression
                                </td>
                                <td bgcolor="#dbac69" style="color:#000;border: 1px solid black;width:15%;">Phần 2<br>
                                    Vocabulary
                                </td>
                                <td bgcolor="#dbac69" style="color:#000;border: 1px solid black;width:15%;">Phần 3 <br>Dictation
                                </td>
                                <td bgcolor="#dbac69" style="color:#000;border: 1px solid black;width:15%;">Phần 4 <br>Listening
                                </td>
                                <td bgcolor="#dbac69" style="color:#000;border: 1px solid black;width:15%;">TỔNG</td>
                            </tr>
                            <tr>
                                <td bgcolor="#dbac69" style="color:#000;border: 1px solid black;text-align:center;"
                                    height="50">ĐIỂM
                                </td>
                                <td style="background-color:#fff;border: 1px solid black;">
                                    <span style="font-size:25px;font-weight:bold;"><?php echo $conver; ?></span>/250
                                </td>
                                <td style="background-color:#fff;border: 1px solid black;">
                                    <span style="font-size:25px;font-weight:bold;"><?php echo $voca; ?></span>/250
                                </td>
                                <td style="background-color:#fff;border: 1px solid black;">
                                    <span style="font-size:25px;font-weight:bold;"><?php echo $dict; ?></span>/250
                                </td>
                                <td style="background-color:#fff;border: 1px solid black;">
                                    <span style="font-size:25px;font-weight:bold;"><?php echo $listen; ?></span>/250
                                </td>
                                <td style="background-color:#fff;border: 1px solid black;">
                                    <span style="font-size:25px;font-weight:bold;"><?php echo $total; ?></span>/1000
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <br><br>
                        <div class="row" style="width: 70%; margin: auto;">
                            <div class="col-md-6" style="text-align: center">
                                <a class="btn btn-primary"
                                   style="background-color: #dbac69; border-color: #dbac69; min-width: 200px"
                                   target="_blank" href="<?php echo $link_home; ?>">Quay lại làm bài</a>
                                <a class="btn btn-primary"
                                   style="background-color: #dbac69; border-color: #dbac69; min-width: 200px; margin-top: 20px;"
                                   target="_blank" href="<?php echo $link_detail; ?>">Xem kết quả</a>
                            </div>
                            <div class="col-md-6" style="text-align: center">
                                <a class="btn btn-danger" target="_blank"
                                   style="background-color: #dbac69; border-color: #dbac69; min-width: 200px"
                                   href="<?php echo $link_view; ?>">Chi tiết bài thi</a>
                                <form action="" method="post" style="text-align: center">
                                    <input type="hidden" name="pdf" value="1">
                                    <button class="btn btn-info"
                                            style="background-color: #dbac69; border-color: #dbac69; min-width: 200px; margin-top: 20px;"
                                            type="submit">Download bảng điểm
                                    </button>
                                </form>
                            </div>
                            <div class="col-md-12" style="text-align: center; margin-top: 20px;">
                                <a class="btn btn-primary"
                                   style="background-color: #adabab; border: none; min-width: 200px"
                                   href="<?php echo $link_logout; ?>">Thoát chương trình</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--End Container-->
    </div><!--End wrapper_container-->

    <div id="wrapper_footer">
        <div id="container" class="footer">
            <div class="footer_logo">
                <a class="">
                    <img src="http://tracnghiem.topicanative.edu.vn/theme/clean/style/img/footer-image.png"
                         style="height:71px;">
                </a>
            </div>
        </div><!--End Container-->
    </div><!--End Footer-->
</div><!--End Page-->
<script type="text/javascript" src="js/rating.js"></script>
<?php } ?>
</body>
</html>
