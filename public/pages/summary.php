<?php
require_once '../../config.php';
require_once '../libs/api.php';

$attemptid = required_param('attempt', PARAM_INT);

$response = call_API($API['api'] . $API['getMarkMonth'], array('attemptid' => $attemptid));

if (boolval($response['success'])) {
    $api_data = $response['data'];
    $topicaMark = $response['data']->marks;
    $topicaMark = json_decode($topicaMark);
    $voca = ceil(doubleval($topicaMark->VOCABULARY));
    $conver = ceil(doubleval($topicaMark->CONVERSATIONAL_EXPRESSION));
    $listen = ceil(doubleval($topicaMark->LISTENING));
    $dict = ceil(doubleval($topicaMark->DICTATION));
    $total = $voca + $conver + $listen + $dict;
    $name = $api_data->firstname . ' ' . $api_data->lastname;
    $username = $api_data->username;
    $date = $api_data->created_at;
    $url_api = $API['api'] . $API['setRating'];
}
?>

<!DOCTYPE html>
<html dir="ltr" lang="en" xml:lang="en">
<head>
    <title></title>
    <link rel="shortcut icon"
          href="http://tracnghiem.topicanative.edu.vn/theme/image.php/clean/theme/1468830243/favicon"/>
    <link rel="stylesheet" type="text/css"
          href="http://tracnghiem.topicanative.edu.vn/theme/clean/style/tracnghiem.css">
    <script type="text/javascript"
            src="http://tracnghiem.topicanative.edu.vn/mod/quiz/js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="http://tracnghiem.topicanative.edu.vn/mod/quiz/js/jquery.plugin.js"></script>
    <script type="text/javascript" src="http://tracnghiem.topicanative.edu.vn/mod/quiz/js/disableBack.js"></script>

    <style>
        #container {
            width: auto;
            font-size: 11px;
        }

        #page {
            background-image: linear-gradient(rgba(255, 255, 255, 0.5), rgba(255, 255, 255, 0.5)), url(img/brgtn.jpeg) !important;
            position: absolute !important;
            width: 100% !important;
            height: 100% !important;
            top: 0 !important;
            left: 0 !important;
            overflow: auto !important;
            background-size: 100% 100% !important;
        }

        #hedder-tn {
            height: 50px;
            background-color: rgba(0, 0, 0, 0.8);
            width: 100%;

            position: absolute;

        }

        #hedder-tn .img-logo {
            height: 50px;
            box-sizing: border-box;
            padding: 10px;
            margin-left: 10%;
        }

        .content span {
            font-size: 16px !important;
        }

        #wrapper_container {
            margin-bottom: 0;
        }

        #welcome {
            width: 50%;
            padding: 10px;
            float: none;
            margin: 0 auto;
            margin-top: 130px;
            font-size: 13px;
            border-bottom: 0;
            box-sizing: border-box;
            background-color: rgba(0, 0, 0, 0.9);
            color: #dbac69;
        }

        #notification {
            margin-top: 0;
            background-color: rgba(0, 0, 0, 0.5) !important;
            color: #dbac69;
            width: 50%;
            margin: 0 auto;
        }

        #notification .note {
            color: #dbac69;
        }

        #footer-tn {
            border-top: 3px solid #dbac69;
            height: 80px;
            background-color: #111;
            width: 100%;

            bottom: 0;
            position: fixed;
            left: 0;
        }

        #footer-tn .gt-topica {
            width: 43%;
            float: left;
            height: 80px;
            color: #dbac69;
            font-size: 13px;
        }

        #footer-tn .content-footer {
            width: 80%;
            height: 80px;
            margin: 0 auto;
            float: none;
        }

        #footer-tn .logo-bottom {
            width: 15%;
            height: 80px;
            box-sizing: border-box;
            padding: 10px;
            float: left;
        }

        #footer-tn .logo-bottom img {
            width: 100%;
            height: auto;
        }

        #footer-tn .contact-footer {
            width: 25%;
            height: 80px;
            float: left;
            color: #dbac69;
        }

        #footer-tn .address-bottom {
            width: 15%;
            height: 80px;
            float: left;
            color: #dbac69;
        }

        #footer-tn .link-footer {
            width: 10%;
            height: 80px;
            float: left;
            color: #dbac69;
        }

        #footer-tn .link-footer p a {
            color: #dbac69;
            text-decoration: none;
        }

        #footer-tn .contact-footer .icon-contact {
            width: 20%;
            height: 80px;
            float: left;
        }

        #footer-tn .contact-footer .icon-contact i {
            float: right;
            margin-top: 10px;
            margin-right: 5px;
        }

        #footer-tn .contact-footer .list-contact {
            width: 80%;
            margin-top: 10px;
            font-size: 12px;
            height: 70px;
            float: left;

        }

        #wrapper_footer {
            display: none !important;
        }

        #wrapper_header {
            display: none !important;
        }
    </style>
</head>

<body>
<div id="page">
    <div id="wrapper_header">
        <div id="container">
            <div class="header_logo">
                <a class="">
                    <img src="http://tracnghiem.topicanative.edu.vn/theme/clean/style/img/logo-topmito.png"
                         style="height:42px;">
                </a>

                <b style="color:#810c15; float:right; font-size: 20px; margin-top: -21px;">
                    Hotline
                    </br>
                    <p style="color:#810c15; float:left; font-size: 15px; margin: 0 auto;">
                        Vietnam: 1800 6885 (nhánh 3)
                    </p>
                    </br>
                    <p style="color:#810c15; float:left; font-size: 15px; margin: 0 auto;">
                        Thailand: +6621054246 (กด 1)
                    </p><!-- thuyvv 29012016 20160922 -->
                    </br>
                    <p style="color:#810c15; float:left; font-size: 15px; margin: 0 auto;">
                        Indonesia: +622129223039
                    </p>
                </b>
                <!-- thuyvv 29012016 -->
            </div><!--End header_logo-->
        </div><!--End header_main-->
    </div><!--End Header-->

    <div id="wrapper_container">
        <div id="container">
            <div class="content">
                <span id="maincontent"></span>
                <link rel='stylesheet' href='http://fontawesome.io/assets/font-awesome/css/font-awesome.css'>
                <div id="container" style="width:800px; margin: 0 auto;margin-top:80px;">
                    <br><br>
                    <div style="text-align:center;">
                        <h1 style="color: #dbac69;font-weight: bolder;padding: 10px;background: rgba(0,0,0,0.8);box-sizing: border-box;text-align: center;margin-bottom:0;">
                            BÁO CÁO ĐIỂM</h1>
                    </div>
                    <div class="content-body"
                         style="overflow:auto;padding: 20px;box-sizing: border-box;margin-bottom:90px;;background: rgba(0,0,0,0.5);">
                        <div style="text-align:center">
                            <p style="color:#fff;">(Nội dung phản hồi về bài kiểm tra)</p>
                        </div>
                        <table style="width:100%; border-collapse: collapse;">
                            <tbody>
                            <tr>
                                <th bgcolor="#dbac69" style="color:#000;border: 1px solid black;width:23%;" height="30">
                                    Họ và tên
                                </th>
                                <th colspan="3"
                                    style="background-color:#fff;color:#000;border: 1px solid black;text-align:left;padding-left:10px;">
                                    <?php echo $name; ?>
                                </th>
                            </tr>
                            <tr>
                                <td bgcolor="#dbac69"
                                    style="color:#000;border: 1px solid black;width:23%;text-align:center;" height="30">
                                    Username
                                </td>
                                <td style="background-color:#fff;border: 1px solid black;width:23%;padding-left:10px;">
                                    <?php echo $username; ?>
                                </td>
                                <td bgcolor="#dbac69"
                                    style="color:#000;border: 1px solid black;width:23%;text-align:center;">Ngày kiểm
                                    tra
                                </td>
                                <td style="background-color:#fff;border: 1px solid black;width:23%;padding-left:10px;">
                                    <?php echo $date; ?>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <br><br>
                        <table style="width:100%; border-collapse: collapse;text-align:center;">
                            <tbody>
                            <tr>
                                <th>Điểm</th>
                                <th colspan="4" style="color:#fff">Hãy kiểm tra điểm bài thi của bạn
                                </th>
                            </tr>
                            <tr>
                                <td bgcolor="#dbac69" style="color:#000;border: 1px solid black;width:15%;">PHẦN</td>
                                <td bgcolor="#dbac69" style="color:#000;border: 1px solid black;width:15%;">Phần 1<br>Conversational
                                    <br>Expression
                                </td>
                                <td bgcolor="#dbac69" style="color:#000;border: 1px solid black;width:15%;">Phần 2<br>
                                    Vocabulary
                                </td>
                                <td bgcolor="#dbac69" style="color:#000;border: 1px solid black;width:15%;">Phần 3 <br>Dictation
                                </td>
                                <td bgcolor="#dbac69" style="color:#000;border: 1px solid black;width:15%;">Phần 4 <br>Listening
                                </td>
                                <td bgcolor="#dbac69" style="color:#000;border: 1px solid black;width:15%;">TỔNG</td>
                            </tr>
                            <tr>
                                <td bgcolor="#dbac69" style="color:#000;border: 1px solid black;text-align:center;"
                                    height="50">ĐIỂM
                                </td>
                                <td style="background-color:#fff;border: 1px solid black;">
                                    <span style="font-size:25px;font-weight:bold;"><?php echo $conver; ?></span>/250
                                </td>
                                <td style="background-color:#fff;border: 1px solid black;">
                                    <span style="font-size:25px;font-weight:bold;"><?php echo $voca; ?></span>/250
                                </td>
                                <td style="background-color:#fff;border: 1px solid black;">
                                    <span style="font-size:25px;font-weight:bold;"><?php echo $dict; ?></span>/250
                                </td>
                                <td style="background-color:#fff;border: 1px solid black;">
                                    <span style="font-size:25px;font-weight:bold;"><?php echo $listen; ?></span>/250
                                </td>
                                <td style="background-color:#fff;border: 1px solid black;">
                                    <span style="font-size:25px;font-weight:bold;"><?php echo $total; ?></span>/1000
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <br><br>
                        <div style="width:40%;float:left;color:#fff;">Mức độ thành thạo trên tổng thể</div>
                        <div style="width:60%o; float: right;color:#fff;">Mức độ thành thạo của bạn được đánh giá qua
                            bảng điểm.
                        </div>
                        <br><br>
                        <table style="width:100%; border-collapse: collapse;text-align:center;">
                            <tbody>
                            <tr>
                                <th bgcolor="#dbac69"
                                    style="color:#000;border: 1px solid black;width:15%;color:#000; height: 30px;">TRÌNH
                                    ĐỘ
                                </th>
                                <th bgcolor="#dbac69" style="color:#000;border: 1px solid black;width:75%;">
                                    ĐÁNH GIÁ
                                </th>
                            </tr>
                            <tr>
                                <td bgcolor="#dbac69" style="color:#000;border: 1px solid black;width:15%;"><p>AA</p>
                                    <p>880 - 1000</p></td>
                                <td rowspan="2"
                                    style="background-color:#fff;text-align:left; border: 1px solid black;padding-left:5px;">
                                    <b>Có khả năng giao tiếp về các chủ đề thường ngày cũng như về các tình uống rộng
                                        hơn trong cuộc sống.</b>
                                    <br>
                                    Có vốn từ vựng đáng kể và khả năng biểu đạt linh hoạt cần thiết trong nhiều tình
                                    huống, về nhiều chủ đề như là kinh tế, thời sự.
                                    <br>
                                    Có kỹ năng sử dụng đúng nhiều từ và nhiều cách diễn đạt khác nhau trong bối cảnh
                                    nhất định.
                                    <br>
                                    Có khả năng giải thích được những câu nói trực tiếp và gián tiếp, đồng thời cũng có
                                    thể suy luận được ý nghĩa cuộc hội thoại.
                                </td>
                            </tr>
                            <tr>
                                <td bgcolor="#dbac69"
                                    style="color:#000;border: 1px solid black;width:15%;color:#000;padding-left:5px;">
                                    <p>A</p>
                                    <p>760 - 879 </p></td>
                            </tr>
                            <tr>
                                <td bgcolor="#dbac69" style="color:#000;border: 1px solid black;width:15%;"><p>B</p>
                                    <p>600 - 759</p></td>
                                <td style="background-color:#fff;text-align:left;border: 1px solid black;padding-left:5px;">
                                    <b>
                                        Có khả năng giao tiếp về các chủ đề thường ngày cũng như các tình huống xã hội
                                        cơ bản.
                                    </b>
                                    <br>
                                    Có vốn từ vựng đáng kể và khả năng biểu đạt linh hoạt cần thiết trong nhiều tình
                                    huống, về nhiều chủ đề thường ngày cũng như các tình huống xã hội cơ bản. Có khả
                                    năng nắm được ý chính và nội dung cụ thể trong cuộc hội thoại tiếng Anh của người
                                    bản địa.
                                </td>
                            </tr>
                            <tr>
                                <td bgcolor="#dbac69" style="color:#000;border: 1px solid black;width:15%;"><p>C</p>
                                    <p>450 - 599</p></td>
                                <td style="background-color:#fff;text-align:left;border: 1px solid black;padding-left:5px;">
                                    <b>
                                        Có khả năng giao tiếp về những chủ đề thường hay gặp trong cuộc sống.
                                    </b>
                                    <br>
                                    Có chút vốn từ vựng và khả năng biểu đạt linh hoạt, đủ dùng trong các giao tiếp hàng
                                    ngày, đôi khi có thể hơi ngại vì vốn từ chưa đa dạng, cũng như chưa biết cách diễn
                                    tả chính xác. Có khả năng nắm được nội dung các cuộc hội thoại hàng ngày.
                                </td>
                            </tr>
                            <tr>
                                <td bgcolor="#dbac69" style="color:#000;border: 1px solid black;width:15%;"><p>D</p>
                                    <p>390 - 449</p></td>
                                <td style="background-color:#fff;text-align:left;border: 1px solid black;padding-left:5px;">
                                    <b>
                                        Có khả năng giao tiếp về các chủ đề cơ bản hàng ngày.
                                    </b>
                                    <br>
                                    Có vốn từ vựng và khả năng biểu đạt đủ để hiểu, nhưng có thể gặp khó khăn khi diễn
                                    đạt trong hội thoại.
                                    <br>
                                    Có khả năng hiểu phần nào cuộc hội thoại, nhưng vẫn chưa đủ để hiểu hết toàn bộ nội
                                    dung.
                                </td>
                            </tr>
                            <tr>
                                <td bgcolor="#dbac69" style="color:#000;border: 1px solid black;width:15%;"><p>E</p>
                                    <p>0 - 389</p></td>
                                <td style="background-color:#fff;text-align:left;border: 1px solid black;padding-left:5px;">
                                    <b>
                                        Có khả năng chào hỏi ở mức độ sơ đẳng, và có thể trả lời các câu hỏi đơn giản.
                                    </b>
                                    <br>
                                    Cần tăng cường vốn từ vựng, khả năng diễn đạt cũng như kỹ năng nghe để có thể giao
                                    tiếp được bằng tiếng Anh.
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <div id="container" style=" margin: 0 auto"><br><br>
                            <table style="width:100%; border-collapse: collapse;text-align:center;">
                                <tbody>
                                <tr>
                                    <th bgcolor="#dbac69"
                                        style="color:#000;border: 1px solid black;width:20%; font-size: 14px;">
                                        Điểm thành phần
                                    </th>
                                    <th bgcolor="#dbac69"
                                        style="color:#000;border: 1px solid black;width:20%; font-size: 14px;">
                                        Phần 1 <br>Conversational<br> Expression
                                    </th>
                                    <th bgcolor="#dbac69"
                                        style="color:#000;border: 1px solid black;width:20%; font-size: 14px;">
                                        Phần 2 <br>Vocabulary
                                    </th>
                                    <th bgcolor="#dbac69"
                                        style="color:#000;border: 1px solid black;width:20%;font-size: 14px;">
                                        Phần 3 <br>Dictation
                                    </th>
                                    <th bgcolor="#dbac69"
                                        style="color:#000;border: 1px solid black;width:20%; font-size: 14px;">
                                        Phần 4 <br>Listening
                                    </th>
                                </tr>

                                <tr>
                                    <td bgcolor="#dbac69" style="color:#000;border: 1px solid black;width:20%;">
                                        186 – 250
                                    </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                        Gần như nắm vững các diễn đạt bằng tiếng Anh cho cuộc sống hàng ngày cũng như
                                        trong giáo tiếp xã hội.
                                    </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                        Có đủ vốn từ vựng để đọc báo, tạp chí và hiểu được hết nội dung được trình bày.
                                    </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                        Có thể nắm được chi tiết nội dung đàm thoại dài của người bản ngữ ở tốc độ
                                        nhanh, về các chủ đề thường ngày và xã hội.
                                    </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                        Có thể nắm được nội dung đàm thoại dài của người bản ngữ ở tốc độ nhanh, về các
                                        chủ đề thường ngày và xã hội.
                                    </td>


                                </tr>

                                <tr>
                                    <td bgcolor="#dbac69" style="color:#000;border: 1px solid black;width:20%;">
                                        151 – 185
                                    </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                        Có thể hiểu được các diễn đạt cơ bản sử dụng trong đời sống hàng ngày cũng như
                                        trong giao tiếp xã hội.
                                    </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                        Có vốn từ vựng đủ để nắm được nội dung cơ bản của câu chuyện và có thể giao tiếp
                                        với người bản địa.
                                    </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                        Có thể nắm được các điểm cơ bản và chi tiết nội dung đàm thoại của người bản ngữ
                                        ở tốc độ vừa phải về những chủ đề tương đối rộng.
                                    </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                        Có thể nắm được tổng quát nội dung đàm thoại của người bản ngữ ở tốc độ vừa
                                        phải, về những chủ đề tương đối rộng
                                    </td>


                                </tr>
                                <tr>
                                    <td bgcolor="#dbac69" style="color:#000;border: 1px solid black;width:20%;">
                                        116 – 150
                                        </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                        Đủ để hiểu được các diễn đạt đơn giản sử dụng trong đời sống hàng ngày
                                    </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                        Có vốn từ vựng cơ bản, đủ cho cuộc sống hàng ngày, ví dụ như để trao đổi ý
                                        tưởng.
                                        </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                        Có thể nắm bắt được các từ khóa và thông tin liên quan trong cuộc đàm thoại của
                                        người bản ngữ ở tốc độ vừa phải.
                                    </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                        Có thể nắm được tổng quát nội dung đàm thoại của người bản ngữ ở tốc độ vừa
                                        phải, về những chủ đề thường ngày.
                                    </td>
                                </tr>


                                <tr>
                                    <td bgcolor="#dbac69" style="color:#000;border: 1px solid black;width:20%;">
                                        96 – 115
                                        </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                        Có thể hiểu được các diễn đạt cơ bản sử dụng trong đời sống hàng ngày
                                    </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                        Có vốn từ vựng rất cơ bản.
                                    </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                        Có thể nắm bắt được các từ khóa và những nội dung cụ thể trong cuộc đàm thoại
                                        hàng ngày của người bản ngữ ở tốc độ chậm.
                                    </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                        Có thể đoán được ý một số phần (như là chủ đề, ý các đoạn) trong cuộc hội thoại
                                        thường ngày của người bản ngữ ở tốc độ chậm.
                                    </td>
                                </tr>

                                <tr>
                                    <td bgcolor="#dbac69" style="color:#000;border: 1px solid black;width:20%;">
                                        0 – 95
                                        </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                        Có thể hiểu được các câu chào hỏi đơn giản, những chủ đề quen thuộc trong đời
                                        sống hàng ngày, và những diễn đạt sơ đẳng.
                                    </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                        Có thể hiểu được các từ quen thuộc sử dụng trong cuộc sống hàng ngày
                                    </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                        Có thể nắm bắt các từ quen thuộc sử dụng hàng ngày như số, ngày trong tuần, được
                                        đề cập trong hội thoại của người bản ngữ ở tốc độ chậm
                                    </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                        Có thể hiểu được các câu chào hỏi cơ bản và đoán sơ được nội dung đàm thoại của
                                        người bản ngữ ở tốc độ chậm.
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--End Container-->
    </div><!--End wrapper_container-->
</div><!--End Page-->
</body>
</html>
