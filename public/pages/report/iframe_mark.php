<html>
<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <script src="https://momentjs.com/downloads/moment.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/autofill/2.3.0/js/dataTables.autoFill.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.0.3/js/buttons.colVis.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/js/bootstrap-datepicker.min.js"></script>
    <style>
        .form-control {
            height: 34px !important;
        }

        .dt-button {
            color: #fff;
            background: #5cb85c;
            border-color: #4cae4c;
            border: 1px solid transparent;
            border-radius: 4px;
        }

        .pagination > .active > span:hover,
        .pagination > .active > a:hover {
            color: #fff !important;
            background: #dbac69 !important;
            border-color: #4cae4c !important;
            border: 1px solid transparent;
        }

        .pagination > .active > a,
        .pagination > .active > a:focus,
        .pagination > .active > span,
        .pagination > .active > span:focus,
        .pagination > li > a, .pagination > li > span {
            color: #fff !important;
            background: #5cb85c !important;
            border-color: #4cae4c !important;
            border: 1px solid transparent;
        }
    </style>
</head>

<body>

<div class="col-md-12"
     style="overflow:auto;padding: 5px;box-sizing: border-box;margin-top: 25px;">
    <div class="col-md-1 text-right"
    >
        <select id="product" class="form-control">
            <option value="1">VN</option>
            <option value="2">VIP</option>
        </select>
    </div>

    <div class="col-md-2 text-right"
    >
        <select id="type_filter" class="form-control">
            <option value="1">Thi tháng</option>
            <option value="2">Thi tuần</option>
            <option value="3">Rating</option>
        </select>
    </div>

    <div class="col-md-2">
        <input id="from_date" type="text" class="form-control" placeholder="From date">
    </div>
    <div class="col-md-2">
        <input id="to_date" type="text" class="form-control" placeholder="To date">
    </div>
    <div class="col-md-2">
        <input id="username" type="text" class="form-control" placeholder="Username">
    </div>
    <div>
        <button id="filter" class="btn btn-success" onclick="submit()">Search</button>
    </div>


</div>
<div id="my_table" class="col-md-12" style="margin-top: 30px">

</div>
</body>
<script>
    $(document).ready(function () {
        $("#from_date").datepicker({format: 'yyyy-mm-dd'});
        // get yesterday by momentjs moment().add(-1, 'days')
        var from_date = $("#from_date").val(moment().add(-1, 'days').format("YYYY-MM-DD"));
        $("#to_date").datepicker({format: 'yyyy-mm-dd'});
        var to_date = $("#to_date").val(moment().format("YYYY-MM-DD"));
        first_load();
    });

    function first_load() {
        var from_date = document.getElementById('from_date').value;
        var to_date = document.getElementById('to_date').value;
        var data = {
            'from_date': from_date,
            'to_date': to_date,
        };
        getTopicaMarkMonth(data);
    }

    function submit() {
        var from_date = document.getElementById('from_date').value;
        var product = document.getElementById('product').value;
        var to_date = document.getElementById('to_date').value;
        var username = document.getElementById('username').value;

        var type_filter = $("select#type_filter").val();
        console.log(type_filter);
        // var quizid = document.getElementById('quiz').value;
        // var levelstudy = document.getElementById('level').value;
        // var status = document.getElementById('status_advisor').value;
        var data = {
            'applicationcode': product,
            'from_date': from_date,
            'to_date': to_date,
            'username': username,
            // 'quizid' : quizid,
            // 'levelstudy' : levelstudy,
            // 'post_adisor' : status
        };

        var data2 = {
            'from_date': from_date,
            'to_date': to_date
        };

        if (type_filter == 1) {
            getTopicaMarkMonth(data);
        } else if (type_filter == 2) {
            getTopicaMarkWeek(data);
        } else if (type_filter == 3) {
            getRating(data2);
        }

    };


    function getTopicaMarkMonth(request_param) {

        document.getElementById("my_table").innerHTML = '<table class="table" id="show_data"><thead><tr id="tb_head"></tr></thead><tbody id="tb_body"></tbody></table>';
        $("#div_loading").removeClass("hidden");
        var tb_head = '';
        tb_head += '<th>' + "Username" + '</th>';
        tb_head += '<th>' + "Userid" + '</th>';
        tb_head += '<th>' + "Attemptid" + '</th>';
        tb_head += '<th>' + "First name" + '</th>';
        tb_head += '<th>' + "Last name" + '</th>';
        tb_head += '<th>' + "Voca" + '</th>';
        tb_head += '<th>' + "Conver" + '</th>';
        tb_head += '<th>' + "Listen" + '</th>';
        tb_head += '<th>' + "Dict" + '</th>';
        tb_head += '<th>' + "Read" + '</th>';
        tb_head += '<th>' + "Gram" + '</th>';
        tb_head += '<th>' + "Total" + '</th>';
        tb_head += '<th>' + "Time finish" + '</th>';
        tb_head += '<th>' + "Level study" + '</th>';

        var tb_body = '';
        $.ajax({
            url: "http://103.56.158.233:1000/api/englishtest/getTopicaMark",
            method: "POST",
            data: request_param,
            success: function (response) {
                if (response.data) {
                    response.data.forEach(function (item) {
                        var marks = JSON.parse(item.marks);
                        var voca = Math.ceil(marks.VOCABULARY);
                        var conver = Math.ceil(marks.CONVERSATIONAL_EXPRESSION);
                        var dict = Math.ceil(marks.DICTATION);
                        var listen = Math.ceil(marks.LISTENING);
                        var gram = Math.ceil(marks.GRAMMAR);
                        var read = Math.ceil(marks.READING);
                        var total = voca + conver + dict + listen + gram + read;
                        tb_body += '<tr>';
                        tb_body += '<td>' + item.username + '</td>';
                        tb_body += '<td>' + item.userid + '</td>';
                        tb_body += '<td>' + item.attemptid + '</td>';
                        tb_body += '<td>' + item.firstname + '</td>';
                        tb_body += '<td>' + item.lastname + '</td>';
                        tb_body += '<td>' + voca + '</td>';
                        tb_body += '<td>' + conver + '</td>';
                        tb_body += '<td>' + listen + '</td>';
                        tb_body += '<td>' + dict + '</td>';
                        tb_body += '<td>' + read + '</td>';
                        tb_body += '<td>' + gram + '</td>';
                        tb_body += '<td>' + total + '</td>';
                        tb_body += '<td>' + item.created_at + '</td>';
                        tb_body += '<td>' + item.levelstudy + '</td>';
                        tb_body += '</tr>';

                    });
                    document.getElementById("tb_head").innerHTML = tb_head;
                    document.getElementById("tb_body").innerHTML = tb_body;

                    var table = $('#show_data').DataTable({
                        buttons: ['excel', 'pdf', 'print'],
                        // dom: '<"top"<"col-md-2"i><"col-md-10"Bfl>>rt<"bottom"p>'
                        "scrollY": 400,
                        "scrollX": true
                    });

                    //add nút export vào datatable
                    table.buttons().container().appendTo('#show_data_wrapper .col-md-6:eq(0)');

                    $("#div_loading").addClass("hidden");
                } else {
                    $("#div_loading").addClass("hidden");
                    tb_body += '<tr class="odd"><td valign="top" colspan="9" style="text-align: center">Không tồn tại bản ghi phù hợp</td></tr>';
                    document.getElementById("tb_body").innerHTML = tb_body;

                }
            }
        });
    }


    function getTopicaMarkWeek(request_param) {

        document.getElementById("my_table").innerHTML = '<table class="table" id="show_data"><thead><tr id="tb_head"></tr></thead><tbody id="tb_body"></tbody></table>';
        $("#div_loading").removeClass("hidden");
        var tb_head = '';
        tb_head += '<th>' + "Username" + '</th>';
        tb_head += '<th>' + "Userid" + '</th>';
        tb_head += '<th>' + "Attemptid" + '</th>';
        tb_head += '<th>' + "First name" + '</th>';
        tb_head += '<th>' + "Last name" + '</th>';
        tb_head += '<th>' + "Mark" + '</th>';
        tb_head += '<th>' + "Quiz Name" + '</th>';
        tb_head += '<th>' + "Level" + '</th>';
        tb_head += '<th>' + "Week" + '</th>';
        tb_head += '<th>' + "Month" + '</th>';
        tb_head += '<th>' + "Year" + '</th>';
        tb_head += '<th>' + "Time Finish" + '</th>';

        console.log(request_param);
        var tb_body = '';
        $.ajax({
            url: "http://103.56.158.233:1000/api/englishtest/getTopicaMarkWeek",
            method: "POST",
            data: request_param,
            success: function (response) {
                if (response.data) {
                    response.data.forEach(function (item) {
                        tb_body += '<tr>';
                        tb_body += '<td>' + item.username + '</td>';
                        tb_body += '<td>' + item.userid + '</td>';
                        tb_body += '<td>' + item.attemptid + '</td>';
                        tb_body += '<td>' + item.firstname + '</td>';
                        tb_body += '<td>' + item.lastname + '</td>';
                        tb_body += '<td>' + item.grade + '</td>';
                        tb_body += '<td>' + item.quiz_name + '</td>';
                        tb_body += '<td>' + item.levelstudy + '</td>';
                        tb_body += '<td>' + item.week + '</td>';
                        tb_body += '<td>' + item.month + '</td>';
                        tb_body += '<td>' + item.year + '</td>';
                        tb_body += '<td>' + item.created_at + '</td>';

                        tb_body += '</tr>';

                    });
                    document.getElementById("tb_head").innerHTML = tb_head;
                    document.getElementById("tb_body").innerHTML = tb_body;

                    var table = $('#show_data').DataTable({
                        buttons: ['excel', 'pdf', 'print'],
                        // dom: '<"top"<"col-md-2"i><"col-md-10"Bfl>>rt<"bottom"p>'
                        "scrollY": 400,
                        "scrollX": true
                    });

                    //add nút export vào datatable
                    table.buttons().container().appendTo('#show_data_wrapper .col-md-6:eq(0)');

                    $("#div_loading").addClass("hidden");
                } else {
                    $("#div_loading").addClass("hidden");
                    tb_body += '<tr class="odd"><td valign="top" colspan="9" style="text-align: center">Không tồn tại bản ghi phù hợp</td></tr>';
                    document.getElementById("tb_body").innerHTML = tb_body;

                }
            }
        });
    }

    function getRating(request_param) {
        document.getElementById("my_table").innerHTML = '<table class="table" id="show_data"><thead><tr id="tb_head"></tr></thead><tbody id="tb_body"></tbody></table>';
        $("#div_loading").removeClass("hidden");
        var tb_head = '';
        tb_head += '<th>' + "Username" + '</th>';
        tb_head += '<th>' + "Attemptid" + '</th>';
        tb_head += '<th>' + "Num star" + '</th>';
        tb_head += '<th>' + "Evaluate1" + '</th>';
        tb_head += '<th>' + "Evaluate2" + '</th>';
        tb_head += '<th>' + "Note" + '</th>';
        tb_head += '<th>' + "Level" + '</th>';
        tb_head += '<th>' + "Created at" + '</th>';
        var tb_body = '';
        $.ajax({
            url: "http://103.56.158.233:1000/api/englishtest/getRating",
            method: "POST",
            data: request_param,
            success: function (response) {
                if (response.message) {
                    response.message.forEach(function (item) {
                        tb_body += '<tr>';
                        tb_body += '<td>' + item.user_name + '</td>';
                        tb_body += '<td>' + item.attemptid + '</td>';
                        tb_body += '<td>' + item.num_star + '</td>';
                        tb_body += '<td>' + item.evaluate1 + '</td>';
                        tb_body += '<td>' + item.evaluate2 + '</td>';
                        tb_body += '<td>' + item.note + '</td>';
                        tb_body += '<td>' + item.type_attempt + '</td>';
                        tb_body += '<td>' + item.created_at + '</td>';
                        tb_body += '</tr>';

                    });
                    document.getElementById("tb_head").innerHTML = tb_head;
                    document.getElementById("tb_body").innerHTML = tb_body;

                    var table = $('#show_data').DataTable({
                        buttons: ['excel', 'pdf', 'print'],
                        // dom: '<"top"<"col-md-2"i><"col-md-10"Bfl>>rt<"bottom"p>'
                        "scrollY": 400,
                        "scrollX": true
                    });

                    //add nút export vào datatable
                    table.buttons().container().appendTo('#show_data_wrapper .col-md-6:eq(0)');

                    $("#div_loading").addClass("hidden");
                } else {
                    $("#div_loading").addClass("hidden");
                    tb_body += '<tr class="odd"><td valign="top" colspan="9" style="text-align: center">Không tồn tại bản ghi phù hợp</td></tr>';
                    document.getElementById("tb_body").innerHTML = tb_body;

                }
            }
        });
    }
</script>
</html>




