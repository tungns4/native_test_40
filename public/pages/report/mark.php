<?php
require_once '../../../config.php';
require_once($CFG->libdir . '/pagelib.php');

require_login();

global $USER, $PAGE;

$username = $USER->username;
$find = 'admin';
$pos = strpos($username, $find);

if($pos === false){
    echo '<script>alert("Bạn không có quyền truy cập mục này");</script>';
    redirect($CFG->wwwroot . '/my/?lang=vi');
}

$PAGE->set_context(get_system_context());
$PAGE->set_pagelayout('standard');
$PAGE->set_url('/public/pages/report/mark.php');
$PAGE->set_title('Topica Report');
$PAGE->set_heading('Report');

$settingnode = $PAGE->settingsnav->add('Topica Report', new moodle_url($CFG->wwwroot . '/public/pages/report/mark.php'), navigation_node::TYPE_CONTAINER);
$thingnode = $settingnode->add('Mark', new moodle_url($CFG->wwwroot . '/public/pages/report/mark.php'));
$thingnode->make_active();

echo $OUTPUT->header();
?>
    <script>
        function resizeIframe(obj) {
            obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
        }
    </script>
    <iframe src="iframe_mark.php" style="width: 100%; min-height: 45rem;" scrolling="no" frameborder="0"
            onload="resizeIframe(this)"></iframe>
<?php

echo $OUTPUT->footer();
?>