var dg_bad = 0;
var dg_good = 0;
var star = 0;
var y_kien = '';

$('.rating .full').click(function (event) {

    star = $(this).attr('data-star');
    $('.submit-star').css('background-color', '#ebebeb');
    $('#start-rating .submit-star').css('display', 'inline-block');
    if (star <= 2) {
        dg_good = 0;
        $(".click_dg .btn").css('box-shadow', '0 0 0 #fff');
        $('#start-rating .tks-gv').css('display', 'block');
        $('#start-rating .st_good').css('display', 'none');
    } else {
        dg_bad = 0;
        $('#y_kien').val('');
        $(".knd").css('background-color', '#ebebeb');
        $('#start-rating .tks-gv').css('display', 'none');
        $('#start-rating .st_good').css('display', 'block');
    }
    switch (star) {
        case '1':
            $('.status-star1').empty().append("Rất không hài lòng");
            break;
        case '2':
            $('.status-star1').empty().append("Không hài lòng");
            break;
        case '3':
            $('.status-star1').empty().append("Bình thường");
            break;
        case '4':
            $('.status-star1').empty().append("Hài lòng");
            break;
        case '5':
            $('.status-star1').empty().append("Rất hài lòng");
            break;
    }
});

$('.rating .full').hover(function () {
    $('.status-star1').css('display', 'none');
    $('.status-star2').css('display', 'inline-block');
    var star1 = $(this).attr('data-star');
    switch (star1) {
        case '1':
            $('.status-star2').empty().append("Rất không hài lòng");
            break;
        case '2':
            $('.status-star2').empty().append("Không hài lòng");
            break;
        case '3':
            $('.status-star2').empty().append("Bình thường");
            break;
        case '4':
            $('.status-star2').empty().append("Hài lòng");
            break;
        case '5':
            $('.status-star2').empty().append("Rất hài lòng");
            break;
    }
});

$('.rating .full').mouseout(function () {
    $('.status-star2').css('display', 'none');
    $('.status-star1').css('display', 'inline-block');
});


$(".tks-itt").click(function () {
    dg_bad = $(this).attr('data-index');
    $(".knd").css('background-color', '#ebebeb');
    $(this).find('.knd').css('background-color', '#dbac69');
    $('.submit-star').css('background-color', '#dbac69');
});

$(".st_good .click_dg .btn").click(function () {
    dg_good = $(this).val();
    $(".click_dg .btn").css('box-shadow', '0 0 0 #fff');
    $(this).css('box-shadow', '2px 2px 1px rgba(0, 0, 0, 0.5)');
    $('.submit-star').css('background-color', '#dbac69');
});

$('.submit-star').click(function () {
    var username = $('.submit-star').attr('data-username');
    var attemptid = $('.submit-star').attr('data-attempt');
    var type_attempt = $('.submit-star').attr('data-type-attempt');
    var link = $('.submit-star').attr('data-api');
    y_kien = $('#y_kien').val();
    if (username != '') {
        if (dg_bad != 0 || dg_good != 0) {
            var data = {
                num_star: star,
                evaluate1: dg_good,
                evaluate2: dg_bad,
                note: y_kien,
                user_name: username,
                attemptid: attemptid,
                type_attempt : type_attempt
            };
            $.ajax({
                url: link,
                type: "POST",
                async: true,
                data: data,
                success: function (result) {
                    if (result != 0) {
                        $(".close").click();
                    } else {
                        alert('Error!');
                    }
                }
            });
        } else {
            alert("Bạn phải chọn giá trị")
        }
    } else {
        alert("Bạn phải đăng nhập!");
    }
});

