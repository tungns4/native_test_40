<?php
require_once '../../config.php';

require_login();

global $USER;

if ($USER->username != 'admin') {
    redirect(wwwroot . '/my/index.php?lang=vi');
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Grade Report</title>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="public/pages/favicon.ico">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/autofill/2.3.0/js/dataTables.autoFill.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>

    <script src="https://momentjs.com/downloads/moment.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/js/bootstrap-datepicker.min.js"></script>

    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/autofill/2.3.0/css/autoFill.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">
    <style>
        #container {
            width: auto;
        }

        #page {
            background-image: linear-gradient(rgba(255, 255, 255, 0.5), rgba(255, 255, 255, 0.5)), url(img/brgtn.jpeg) !important;
            position: absolute !important;
            width: 100% !important;
            height: 100% !important;
            top: 0 !important;
            left: 0 !important;
            overflow: auto !important;
            background-size: 100% 100% !important;
        }

        #hedder-tn {
            height: 50px;
            background-color: rgba(0, 0, 0, 0.8);
            width: 100%;

            position: absolute;

        }

        #hedder-tn .img-logo {
            height: 50px;
            box-sizing: border-box;
            padding: 10px;
            margin-left: 10%;
        }

        .content span {
            font-size: 16px !important;
        }

        #wrapper_container {
            margin-bottom: 0;
        }

        #welcome {
            width: 50%;
            padding: 10px;
            float: none;
            margin: 0 auto;
            margin-top: 130px;
            font-size: 13px;
            border-bottom: 0;
            box-sizing: border-box;
            background-color: rgba(0, 0, 0, 0.9);
            color: #dbac69;
        }

        #notification {
            margin-top: 0;
            background-color: rgba(0, 0, 0, 0.5) !important;
            color: #dbac69;
            width: 50%;
            margin: 0 auto;
        }

        #notification .note {
            color: #dbac69;
        }

        #footer-tn {
            border-top: 3px solid #dbac69;
            height: 80px;
            background-color: #111;
            width: 100%;

            bottom: 0;
            position: fixed;
            left: 0;
        }

        #footer-tn .gt-topica {
            width: 43%;
            float: left;
            height: 80px;
            color: #dbac69;
            font-size: 13px;
        }

        #footer-tn .content-footer {
            width: 80%;
            height: 80px;
            margin: 0 auto;
            float: none;
        }

        #footer-tn .logo-bottom {
            width: 15%;
            height: 80px;
            box-sizing: border-box;
            padding: 10px;
            float: left;
        }

        #footer-tn .logo-bottom img {
            width: 100%;
            height: auto;
        }

        #footer-tn .contact-footer {
            width: 25%;
            height: 80px;
            float: left;
            color: #dbac69;
        }

        #footer-tn .address-bottom {
            width: 15%;
            height: 80px;
            float: left;
            color: #dbac69;
        }

        #footer-tn .link-footer {
            width: 10%;
            height: 80px;
            float: left;
            color: #dbac69;
        }

        #footer-tn .link-footer p a {
            color: #dbac69;
            text-decoration: none;
        }

        #footer-tn .contact-footer .icon-contact {
            width: 20%;
            height: 80px;
            float: left;
        }

        #footer-tn .contact-footer .icon-contact i {
            float: right;
            margin-top: 10px;
            margin-right: 5px;
        }

        #footer-tn .contact-footer .list-contact {
            width: 80%;
            margin-top: 10px;
            font-size: 12px;
            height: 70px;
            float: left;

        }

        #wrapper_footer {
            display: none !important;
        }

        #wrapper_header {
            display: none !important;
        }
    </style>
</head>
<body>
<div id="page">
    <div id="wrapper_container">
        <div id="container">
            <div class="content">

                <script>
                    $(document).ready(function () {
                        $("#from_date").datepicker({format: 'yyyy-mm-dd'});
                        $("#from_date").val(moment().format("YYYY-MM-DD"));
                        $("#to_date").datepicker({format: 'yyyy-mm-dd'});
                        $("#to_date").val(moment().format("YYYY-MM-DD"));
                        getTopicaMark();
                    });

                    function submit() {
                        var from_date = document.getElementById('from_date').value;
                        var product = document.getElementById('product').value;
                        var to_date = document.getElementById('to_date').value;
                        var username = document.getElementById('username').value;
                        var data = {
                            'applicationcode': product,
                            'from_date': from_date,
                            'to_date': to_date,
                            'username': username
                        };

                        getTopicaMark(data);
                    };


                    function getTopicaMark(request_param) {

                        document.getElementById("my_table").innerHTML = '<table class="table" id="show_data"><thead><tr id="tb_head"></tr></thead><tbody id="tb_body"></tbody></table>';
                        $("#show_data").removeClass("datatable");
                        $("#div_loading").removeClass("hidden");

                        $.ajax({
                            url: "http://103.56.158.233:1000/api/englishtest/getTopicaMark",
                            method: "POST",
                            data: request_param,
                            success: function (response) {

                                if (response.data) {

                                    var tb_head = '';
                                    tb_head += '<th>' + "Username" + '</th>';
                                    tb_head += '<th>' + "Voca" + '</th>';
                                    tb_head += '<th>' + "Conver" + '</th>';
                                    tb_head += '<th>' + "Listen" + '</th>';
                                    tb_head += '<th>' + "Dict" + '</th>';
                                    tb_head += '<th>' + "Read" + '</th>';
                                    tb_head += '<th>' + "Gram" + '</th>';
                                    tb_head += '<th>' + "Total" + '</th>';
                                    tb_head += '<th>' + "Time finish" + '</th>';

                                    document.getElementById("tb_head").innerHTML = tb_head;
                                    var tb_body = '';
                                    var stt = 1;
                                    response.data.forEach(function (item) {
                                        var marks = JSON.parse(item.marks);
                                        var voca = Math.ceil(marks.VOCABULARY);
                                        var conver = Math.ceil(marks.CONVERSATIONAL_EXPRESSION);
                                        var dict = Math.ceil(marks.DICTATION);
                                        var listen = Math.ceil(marks.LISTENING);
                                        var gram = Math.ceil(marks.GRAMMAR);
                                        var read = Math.ceil(marks.READING);
                                        var total = voca + conver + dict + listen + gram + read;
                                        tb_body += '<tr>';
                                        tb_body += '<td>' + item.username + '</td>';
                                        tb_body += '<td>' + voca + '</td>';
                                        tb_body += '<td>' + conver + '</td>';
                                        tb_body += '<td>' + listen + '</td>';
                                        tb_body += '<td>' + dict + '</td>';
                                        tb_body += '<td>' + read + '</td>';
                                        tb_body += '<td>' + gram + '</td>';
                                        tb_body += '<td>' + total + '</td>';
                                        tb_body += '<td>' + item.created_at + '</td>';
                                        tb_body += '</tr>';

                                    });
                                    document.getElementById("tb_body").innerHTML = tb_body;

                                    var table = $('#show_data').DataTable({
                                        buttons: ['excel', 'pdf', 'print'],
                                        dom: '<"top"<"col-md-2"i><"col-md-10"Bfl>>rt<"bottom"p>'
                                    });

                                    //add nút export vào datatable
                                    table.buttons().container().appendTo('#show_data_wrapper .col-md-6:eq(0)');


                                    $("#div_loading").addClass("hidden");
                                } else {
                                    $("#div_loading").addClass("hidden");
                                }
                            }
                        });
                    }


                </script>
                <!-- Modal -->


                <div style="width: 95%; margin: 0 auto;margin-top:30px; border: 1px black solid; background: lightgrey;">
                    <div class="col-md-12"
                         style="overflow:auto;padding: 5px;box-sizing: border-box;margin-top: 25px;">
                        <div class="col-md-2 text-right"
                        >
                            <select id="product" class="form-control">
                                <option value="1">NATIVE VN</option>
                                <option value="2">NATIVE VIP</option>
                            </select>
                        </div>

                        <div class="col-md-2">
                            <input id="from_date" type="text" class="form-control" placeholder="From date">
                        </div>
                        <div class="col-md-2">
                            <input id="to_date" type="text" class="form-control" placeholder="To date">
                        </div>
                        <div class="col-md-2">
                            <input id="username" type="text" class="form-control" placeholder="Username">
                        </div>
                        <div>
                            <button id="filter" onclick="submit()">Search</button>
                        </div>
                    </div>
                    <div id="my_table" style="margin-top: 50px">

                    </div>
                </div>
            </div>
        </div>
    </div><!--End Container-->
</div><!--End wrapper_container-->
</div><!--End Page-->
<script type="text/javascript" src="js/rating.js"></script>
</body>
</html>