<?php
require_once '../../config.php';
require_once '../libs/api.php';
$attemptid = required_param('attempt', PARAM_INT);
$request_param = array('attemptid' => $attemptid);
$response = call_API($API['api'] . $API['getMarkWeek'], $request_param);
$data = $response["data"];
//tungns4
$wrong_word = 0;
$right_word = 0;
$wpm = 0;
$grade_part_1=0;
$grade_part_2=0;
$grade_part_3=0;

$wpm_part_2=0;
$wpm_part_3=0;
$wpm_part_4=0;

$before_word ="Bạn đã nhớ và phát âm được đúng các từ:";
$word = "";
$before_grammar ="Bạn đã nhớ và sử dụng được đúng các cấu trúc:";
$before_grammar1 ="Bạn đã nhớ và sử dụng được đúng các cấu trúc:";
$before_grammar2 ="Bạn đã nhớ và sử dụng được đúng các cấu trúc:";
$grammar = "";
$grade = round($data->grade, 2);
$grade_speaking = 0;

if($data->quiz_name == "speaking" || $data->quiz_name == "Speaking"){
    global $DB;
    
    $sql = "SELECT q.name ,ra.* FROM top_result_ai ra INNER JOIN top_question q on ra.question_id = q.id WHERE attempt_id = $attemptid ";
    $data_ai = $DB->get_records_sql($sql,array());
    // echo "<pre>";
    // var_dump($data_ai);die;
    if($data_ai != false){
        $dem = 0;
        
        foreach ($data_ai as $key => $value) {
            // echo "<pre>";

            // var_dump($value) ;
            if($value->wpm != null ){
                $dem += 1;
                $tam_wpm += $value->wpm;
            }
            if(substr($value->name, 0,2) == "P1"){
                $grade_part_tam_1 += $value->ratio;
                $word .= $value->right_word." ";
                $grade_speaking_tam +=$value->ratio*100;
                $wrong_word += count(json_decode($value->wrong_word));
                $right_word += count(json_decode($value->right_word));
            }
            if(substr($value->name, 0,2) == "P2"){
                $grade_part_tam_2 +=($value->ratio);
                $wpm_tam_part_2 += $value->wpm;
                $grade_speaking_tam +=$value->ratio;
                if($value->compare_text != NULL || $value->compare_text != "") {
                    $grammar_tam1.= " - ".str_replace("cấu trúc ","",$value->compare_text)."</br>";
                }
                $wrong_word += count(json_decode($value->wrong_word));
                $right_word += count(json_decode($value->right_word));
            }
            if(substr($value->name, 0,2) == "P3"){
                $grade_part_tam_3  += ($value->ratio);
                $wpm_tam_part_3 += $value->wpm;
                $grade_speaking_tam +=$value->ratio;
                $wrong_word += count(json_decode($value->wrong_word));
                $right_word += count(json_decode($value->right_word));
            }
            if(substr($value->name, 0,2) == "P4"){
                $wpm_tam_part_4 += $value->wpm;
                if($value->student_tense == "Dùng đúng thời"){
                    $grammar_tam2.= " - ".$value->right_tense."</br>";
                }
            }
        } ;

        if(!$grammar_tam2 ){
            $before_grammar2 = "Bạn chưa sử dụng được thời nào";
        }
        if(!$grammar_tam1 ){
            $before_grammar1 = "Bạn chưa sử dụng được cấu trúc nào";
        }
        if($word == ""){
            $before_word ="";
            $word = "Phần 1 bạn không trả lời đúng từ nào ";
        }
        $grammar = $grammar_tam1.$grammar_tam2;
        $grade_speaking =round($grade_speaking_tam/13, 0) ;
        $grade_part_1 = round(100* $grade_part_tam_1/6,0);
        $grade_part_2 = round($grade_part_tam_2/4,0);
        $grade_part_3 = round($grade_part_tam_3/3,0);
        $wpm_part_2=    round($wpm_tam_part_2/4,0);
        $wpm_part_3=    round($wpm_tam_part_3/3,0);
        $wpm_part_4=    round($wpm_tam_part_4/2,0);
        if($tam_wpm !=NULL){
            $wpm = round($tam_wpm /$dem,0) ;
        }
        $pronoun_level = $DB->get_record_sql("SELECT * FROM top_speaking_description WHERE point_min <= $grade_speaking AND point_max > $grade_speaking AND criteria = 'pronoun_level'",array());
        $fluency_level = $DB->get_record_sql("SELECT * FROM top_speaking_description WHERE point_min <= $wpm AND point_max > $wpm AND criteria = 'fluency_level'",array());     
    }else{
        $before_word ="";
        $word = "Bạn không trả lời đúng từ nào";
    }
    
}

$table_content_default= "<table style=\"width:100%; border-collapse: collapse;\">
                    <tbody>
                        <tr>
                            <th bgcolor=\"#dbac69\" style=\"color:#000;border: 1px solid black;width:30%;\" height=\"30\">
                                Họ và tên
                            </th>
                            <th colspan=\"3\"
                                style=\"background-color:#fff;color:#000;border: 1px solid black;text-align:left;padding-left:10px;\">
                                $data->firstname $data->lastname
                            </th>
                        </tr>
                        <tr>
                            <td bgcolor=\"#dbac69\"
                                style=\"color:#000;border: 1px solid black;width:30%;text-align:center;\" height=\"30\">
                                Username
                            </td>
                            <td style=\"background-color:#fff;border: 1px solid black;width:30%;padding-left:10px;\">
                                $data->username
                            </td>
                            <td bgcolor=\"#dbac69\"
                                style=\"color:#000;border: 1px solid black;width:30%;text-align:center;\">Ngày kiểm
                                tra
                            </td>
                            <td style=\"background-color:#fff;border: 1px solid black;width:30%;padding-left:10px;\">
                                $data->created_at
                            </td>
                        </tr>
                        </tbody>
                        </table>
                        <br><br>
                        <table style=\"width:100%; border-collapse: collapse;text-align:center;\">
                        <tbody>
                        <tr>
                            <th>Điểm</th>
                            <th colspan=\"4\" style=\"color:#fff\">Hãy kiểm tra điểm bài thi của bạn
                            </th>
                        </tr>
                        <tr>
                            <td bgcolor=\"#dbac69\" style=\"color:#000;border: 1px solid black;width:30%;\">PHẦN</td>
                            <td bgcolor=\"#dbac69\"
                                style=\"color:#000;border: 1px solid black;width:70%;\">$data->quiz_name</td>
                        </tr>
                        <tr>
                            <td bgcolor=\"#dbac69\" style=\"color:#000;border: 1px solid black;text-align:center;\"
                                height=\"50\">ĐIỂM
                            </td>
                            <td style=\"background-color:#fff;border: 1px solid black;\">
                                <span style=\"font-size:25px;font-weight:bold;\">$grade</span>/$data->sumgrades
                            </td>
                        </tr>
                    </tbody>
                </table>";

$table_content_speaking ="<table style=\"width:100%; border-collapse: collapse; font-size : 15px\">
    <tbody>
        <tr>
            <th bgcolor=\"#dbac69\" class =\"name\" height=\"30\">
                Họ và tên
            </th>
            <th colspan=\"3\"
                style=\"background-color:#fff;color:#000;border: 1px solid black;text-align:left;padding-left:10px;text-align:center;\">
                $data->firstname $data->lastname
            </th>
        </tr>
        <tr>
            <td bgcolor=\"#dbac69\" class =\"name\">
                <b>Username</b>
            </td>
            <td style=\"background-color:#fff;border: 1px solid black;width:25%;padding-left:10px;text-align:center;\">
                $data->username
            </td>
            <td bgcolor=\"#dbac69\"
                style=\"color:#000;border: 1px solid black;width:25%;text-align:center;\">Ngày kiểm
                tra
            </td>
            <td style=\"background-color:#fff;border: 1px solid black;width:25%;padding-left:10px;text-align:center;\">
                $data->created_at
            </td>
        </tr>
        </tbody>
        </table>
        <br><br>
        <table style=\"width:100%; border-collapse: collapse;text-align:center;\">
        <tbody>
        <tr>
            <td bgcolor=\"#dbac69\" class =\"table_content_speaking\" height=\"50\">
                <b>Điểm</b>
            </td>
            <td bgcolor=\"#dbac69\" class =\"table_content_speaking\" >
                <b>Tốc độ nói</b>
            </td>
            <td bgcolor=\"#dbac69\" class =\"table_content_speaking\">
                <b>Số từ đúng</b>
            </td>
            <td bgcolor=\"#dbac69\" class =\"table_content_speaking\">
                <b>Số từ sai</b>
            </td>
            
        </tr>
        <tr>
            <td style=\"background-color:#fff;border: 1px solid black;\" height=\"50\">
                <span style=\"font-size:20px;\">$grade_speaking (%)</span>
            </td>
            <td style=\"background-color:#fff;border: 1px solid black;\">
                <span style=\"font-size:20px;\">$wpm (từ/phút)</span>
            </td>
            <td style=\"background-color:#fff;border: 1px solid black;\">
                <span style=\"font-size:20px;\">$right_word (từ)</span>
            </td>
            <td style=\"background-color:#fff;border: 1px solid black;\">
                <span style=\"font-size:20px;\">$wrong_word (từ)</span>
            </td>
        </tr>
    </tbody>
</table>";
$table_report_speaking = "
    <table style=\"width:100%; border-collapse: collapse;text-align:center;\">
        <tbody>
            <tr>
                <td bgcolor=\"#dbac69\" class =\"criteria\" height=\"50\" >
                    <b>Tiêu chí</b>
                </td>
                <td bgcolor=\"#dbac69\" class =\"description\" >
                    <b>Mô tả</b>
                </td>
                <td bgcolor=\"#dbac69\" class =\"overall\">
                    <b>Tổng quan</b>
                </td>
                <td bgcolor=\"#dbac69\" class =\"report_part\">
                    <b>Phần 1</b>
                </td>
                <td bgcolor=\"#dbac69\" class =\"report_part\" >
                    <b>Phần 2</b>
                </td>
                <td bgcolor=\"#dbac69\" class =\"report_part\" >
                    <b>Phần 3</b>
                </td>
                <td bgcolor=\"#dbac69\" class =\"report_part\">
                    <b>Phần 4</b>
                </td>
            </tr>
            <tr>
                <td style=\"background-color:#fff;border: 1px solid black;\" height=\"50\">
                    <b class=\"title\" >Phát âm</b>
                </td>
                <td style=\"background-color:#fff;border: 1px solid black;\">
                    <b class=\"padding\" style=\"float: left;text-align: justify;\">
                        Tiêu chí này nhằm đánh giá khả năng phát âm chính xác của bạn, được thể hiện qua tỷ lệ % trùng khớp với đáp án. 
                        <br><br>
                        <i>Lưu ý: Bạn sẽ được đánh giá tiêu chí này với các câu hỏi từ phần 1 đến phần 3.</i>
                    </b>
                </td>
                <td style=\"background-color:#fff;border: 1px solid black; \">
                    <span  style=\"font-size:16px;\">Bạn trả lời chính xác trung bình </br> <b style =\"font-size:16px;\"> $grade_speaking %.</b></span> 
                    </br></br>
                    <span class=\"padding\" style=\"float: left;text-align: justify;\"> $pronoun_level->description </span>
                </td>
                <td style=\"background-color:#fff;border: 1px solid black;\">
                    <span style=\"font-size:16px;\">Bạn trả lời chính xác trung bình </br> $grade_part_1 %.</span>
                </td>
                <td style=\"background-color:#fff;border: 1px solid black;\">
                    <span style=\"font-size:16px;\">Bạn trả lời chính xác trung bình </br> $grade_part_2 %.</span>
                </td>
                <td style=\"background-color:#fff;border: 1px solid black;\">
                    <span style=\"font-size:16px;\">Bạn trả lời chính xác trung bình </br> $grade_part_3 %.</span>
                </td>
                <td style=\"background-color:#c3bcbc;border: 1px solid black;\">
                    <span style=\"font-size:16px;\"></span>
                </td>
            </tr>
            <tr>
                <td style=\"background-color:#fff;border: 1px solid black;\" height=\"50\">
                    <b class=\"title\">Trôi chảy</b>
                </td>
                <td style=\"background-color:#fff;border: 1px solid black;\">
                    <b class=\"padding\" style=\"float: left;text-align: justify;\">
                        Tiêu chí này nhằm đánh giá tốc độ nói của bạn, được thể hiện qua đơn vị số từ/phút.
                        <br><br>
                        <i>Lưu ý: Bạn sẽ được đánh giá tiêu chí này với các câu hỏi từ phần 2 đến phần 4.</i>
                    </b>
                </td>
                <td style=\"background-color:#fff;border: 1px solid black;\">
                    <span style=\"font-size:16px;\">Tốc độ nói trung bình của bạn là </br> <b style =\"font-size:16px;\"> $wpm (từ/phút).</b></span> 
                    </br></br>
                    <span class=\"padding\" style=\"float: left;text-align: justify;\"> $fluency_level->description </span>
                </td>
                <td style=\"background-color:#c3bcbc;border: 1px solid black;\">
                    <span style=\"font-size:16px;\"></span>
                </td>
                <td style=\"background-color:#fff;border: 1px solid black;\">
                    <span style=\"font-size:16px;\">Tốc độ nói của bạn là </br> $wpm_part_2 từ/phút.</span>
                </td>
                <td style=\"background-color:#fff;border: 1px solid black;\">
                    <span style=\"font-size:16px;\">Tốc độ nói của bạn là </br> $wpm_part_3 từ/phút.</span>
                </td>
                <td style=\"background-color:#fff;border: 1px solid black;\">
                    <span style=\"font-size:16px;\">Tốc độ nói của bạn là </br> $wpm_part_4 từ/phút.</span>
                </td>
            </tr>
            <tr>
                <td style=\"background-color:#fff;border: 1px solid black;\" height=\"50\">
                    <b class=\"title\">Từ vựng</b>
                </td>
                <td style=\"background-color:#fff;border: 1px solid black;\">
                    <b class=\"padding\" style=\"float: left;text-align: justify;\">
                        Tiêu chí này nhằm đánh giá khả năng ghi nhớ từ vựng của bạn.
                        <br><br>
                        <i>Lưu ý: Bạn sẽ được đánh giá tiêu chí này tại duy nhất phần 1.</i>
                    </b>
                </td>
                <td class=\"padding\" style=\"background-color:#fff;border: 1px solid black;\">
                    <span  style=\"font-size:16px;\">$before_word</br>  $word</span> 
                </td>
                <td class=\"padding\" style=\"background-color:#fff;border: 1px solid black;\">
                    <span style=\"font-size:16px;\"> $before_word </br>  $word  </span>
                </td>
                <td class=\"padding\" style=\"background-color:#c3bcbc;border: 1px solid black;\">
                    <span style=\"font-size:16px;\"></span>
                </td>
                <td class=\"padding\" style=\"background-color:#c3bcbc;border: 1px solid black;\">
                    <span style=\"font-size:16px;\"></span>
                </td>
                <td class=\"padding\" style=\"background-color:#c3bcbc;border: 1px solid black;\">
                    <span style=\"font-size:16px;\"></span>
                </td>
            </tr>
            <tr>
                <td style=\"background-color:#fff;border: 1px solid black;\" height=\"50\">
                    <b class=\"title\">Ngữ pháp</b>
                </td>
                <td class=\"padding\" style=\"background-color:#fff;border: 1px solid black;\">
                    <b style=\"float: left;text-align: justify;\">
                        Tiêu chí này nhằm đánh giá khả năng sử dụng ngữ pháp của bạn.
                        <br><br>
                        <i>Lưu ý: Bạn sẽ được đánh giá tiêu chí này tại phần 2 .</i>
                    </b>
                </td>
                <td class=\"padding\" style=\"background-color:#fff;border: 1px solid black;\">
                    <span style=\"font-size:16px;\">$before_grammar</br>$grammar</span> 
                </td>
                <td class=\"padding\" style=\"background-color:#c3bcbc;border: 1px solid black;\">
                    <span style=\"font-size:16px;\"></span>
                </td>
                <td  class=\"padding\" style=\"background-color:#fff;border: 1px solid black;text-align: justify;\">
                    <span style=\"font-size:16px;\">$before_grammar1</br>$grammar_tam1</span>
                </td>
                <td style=\"background-color:#c3bcbc;border: 1px solid black;\">
                    <span style=\"font-size:16px;\"></span>
                </td>
                <td class=\"padding\" style=\"background-color:#fff;border: 1px solid black;text-align: justify;\">
                    <span style=\"font-size:16px;\">$before_grammar2</br>$grammar_tam2</span>
                </td>
            </tr>
            
        </tbody>
    </table>";
//end tungns4  
                
$table_consider = "<table style=\"width:100%; border-collapse: collapse;text-align:center;\">
                                <tbody>
                                <tr style=\"height: 50px\">
                                    <th bgcolor=\"#dbac69\"
                                        style=\"color:#000;border: 1px solid black;width:20%; font-size: 14px;\">
                                        Trình độ
                                    </th>
                                    <th bgcolor=\"#dbac69\"
                                        style=\"color:#000;border: 1px solid black;width:80%; font-size: 14px;\">
                                        Đánh giá
                                    </th>
                                </tr>
                                <tr>
                                    <td bgcolor=\"#dbac69\" style=\"color:#000;border: 1px solid black;width:20%;\">
                                        0 - 9 câu đúng
                                    </td>
                                    <td style=\"background-color:#fff;border: 1px solid black;width:80%; text-align: left;padding-left:5px;\">
                                        Thật đáng báo động! Vốn kiến thức tiếng anh của bạn thực sự cần trau dồi và học
                                        hỏi hơn rất nhiều. Hãy vạch ra cho bản thân một kế hoạch học tập phù hợp ngay từ
                                        ngày hôm nay để bù lại những kiến thức đã bỏ lỡ tại chương trình. Nếu cần hỗ
                                        trợ, bạn có thể liên hệ với Quản lý học tập để nhận được sự tư vấn và hỗ trợ
                                        ngay nhé!
                                        </br><b>It doesn’t matter how slowly you go as long as you do not stop</b>
                                    </td>
                                </tr>

                                <tr>
                                    <td bgcolor=\"#dbac69\" style=\"color:#000;border: 1px solid black;width:20%;\">
                                        10 - 16 câu đúng
                                    </td>
                                    <td style=\"background-color:#fff;border: 1px solid black;width:80%; text-align: left;padding-left:5px;\">
                                        Hãy cùng Topica đặt ra cho bản thân một kế hoạch học tập và xây dựng ý chí quyết
                                        tâm học hỏi ngay từ ngày hôm nay. Mức điểm của bạn đang ở mức độ còn khiêm tốn
                                        và chưa thực sự tốt. Ngoài những bài học trên lớp, bạn nên dành thời gian luyện
                                        tập các bài tập tự học và mở rộng kiến thức mới. Liên hệ với Quản lý học tập
                                        ngay nếu cần hỗ trợ nhé!
                                        </br></br>
                                    </td>
                                </tr>

                                <tr>
                                    <td bgcolor=\"#dbac69\" style=\"color:#000;border: 1px solid black;width:20%;\">
                                        17 - 24 câu đúng
                                    </td>
                                    <td style=\"background-color:#fff;border: 1px solid black;width:80%; text-align: left;padding-left:5px;\">
                                        Kết quả bài thi tuần của bạn khá tốt. Các kiến thức Topica cung cấp đã được trau
                                        dồi và học hỏi rất tốt. Hãy phát huy hơn nữa trong các tuần tới đây để cố gắng
                                        làm chủ các kiến thức hàng tuần. Đừng dừng lại và hài lòng với mức điểm này, cố
                                        gắng đặt mục tiêu cho tuần tới là đạt số điểm cao hơn nữa để phấn đấu nhé!
                                        </br></br>
                                    </td>
                                </tr>

                                <tr>
                                    <td bgcolor=\"#dbac69\" style=\"color:#000;border: 1px solid black;width:20%;\">
                                        25 - 30 câu đúng
                                    </td>
                                    <td style=\"background-color:#fff;border: 1px solid black;width:80%; text-align: left;padding-left:5px;\">
                                        <b>Congratulation!</b> Thực sự ghi nhận tinh thần học tập của bạn tại chương
                                        trình trong tuần qua. Kết quả bài thi của bạn thực sự rất tốt và đáng được ghi
                                        nhận. Hãy cứ cố gắng và học tập chăm chỉ hơn nữa trong thời gian tới để biến mục
                                        tiêu Giao tiếp tiếng anh thành thạo chỉ còn là chuyện nhỏ, để tiếng anh sẽ là
                                        chià khóa giúp bạn thành công trong thời gian tới.
                                        </br></br>
                                    </td>
                                </tr>
                                </tbody>
                            </table>";

$table_element_inter = '                            <table style="width:100%; border-collapse: collapse;text-align:center;">
                                <tbody>
                                <tr style="height: 50px">
                                    <th bgcolor="#dbac69" style="color:#000;border: 1px solid black;width:20%; font-size: 14px;">
                                        Điểm thành phần
                                    </th>
                                    <th bgcolor="#dbac69" style="color:#000;border: 1px solid black;width:20%; font-size: 14px;">
                                        Sử dụng từ vựng
                                    </th>
                                    <th bgcolor="#dbac69" style="color:#000;border: 1px solid black;width:20%; font-size: 14px;">
                                        Sử dụng ngữ pháp
                                    </th>
                                    <th bgcolor="#dbac69" style="color:#000;border: 1px solid black;width:20%;font-size: 14px;">
                                        Nghe
                                    </th>
                                    <th bgcolor="#dbac69" style="color:#000;border: 1px solid black;width:20%; font-size: 14px;">
                                        Cụm động từ/ Thành ngữ
                                    </th>
                                </tr>
                                <tr>
                                    <td bgcolor="#dbac69" style="color:#000;border: 1px solid black;width:20%;">
                                        Đúng 0 - 4: Kém
                                    </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                        Khả năng sử dụng từ vựng của bạn rất đáng lo ngại, bạn cần liên hệ với quản lý học tập để nhận được hỗ trợ tốt nhất. Ngoài ra bạn cần dành thời gian nhiều hơn cho việc học ở trên lớp và tự học trên kho học liệu của Topica, ngoài các bài học trên lớp nhé!
                                        </br><b>Tell me and I forget. Teach me and I remember. Involve me and I learn</b>
                                    </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                        Vốn ngữ pháp của bạn còn khá hạn chế, bạn cần trau dồi và luyện tập cải thiện ngữ pháp nhiều hơn nữa để có thể tự tin hơn khi giao tiếp tiếng anh. Nếu gặp khó khăn trong việc định hướng và tìm phương pháp học ngữ pháp tốt, hãy liên hệ với quản lý học tập để nhận hỗ trợ tốt nhất.
                                        </br><b>It doesn’t matter how slowly you go as long as you do not stop</b>
                                    </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                        Kỹ năng nghe của bạn thực sự đáng lo ngại. Bạn cần dành thời gian luyện nghe nhiều hơn nữa để giúp bản thân tự tin hơn khi giao tiếp tiếng anh. Sẽ thực sự khó trong giao tiếp khi mình không thể nghe được đối phương nói gì phải không nào? Đừng chần chờ gì nữa, hãy lên kế hoạch học tập cải thiện kỹ năng nghe ngay thôi.
                                        </br><b>Ever tried. Ever failed. No matter. Try again. Fail again. Fail better – Samuel Beckett</b>
                                    </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                        Vốn kiến thức về cụm động từ/thành ngữ của bạn còn rất hạn chế. Cụm động từ hay thành ngữ đều là những câu nói tiếng Anh làm toát lên chất tự nhiên của bạn khi giao tiếp. Do vậy, hãy dành thời gian luyện và học thêm các cụm động từ/thành ngữ để có thể giao tiếp tiếng Anh một cách tự nhiên nhất nhé.
                                        </br><b>Learning is like rowing upstream, not to advance is to drop back</b>
                                    </td>
                                </tr>

                                <tr>
                                    <td bgcolor="#dbac69" style="color:#000;border: 1px solid black;width:20%;">
                                        Đúng 5 - 8: Trung bình
                                    </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                        Bạn cần cố gắng hơn nữa trong việc học tập và ghi nhớ từ vựng. Kết quả bài kiểm tra của bạn còn ở mức khiêm tốn nên rất cần sự nỗ lực và quyết tâm hơn nữa trong việc học từ mới. Ngoài những kiến thức bài học từ phía nhà trường, bạn nên mở rộng thêm từ các bài tập tự học trong học liệu quốc tế, thư viện mở rộng.... để ghi nhớ từ vựng nhiều hơn nhé!
                                    </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                        Kết thúc tuần này, Ngữ pháp của bạn đã đạt mức điểm khá. Mặc dù vậy, bạn vẫn cần luyện tập và học thêm nhiều kiến thức về phần ngữ pháp để giúp bản thân có thể tự tin hơn nữa khi giao tiếp tiếng anh. Hãy liên hệ với quản lý học tập ngay nếu như cần hỗ trợ hay tư vấn về phương pháp học cũng như cách thức học ngữ pháp để đạt hiệu quả cao nhất nhé!
                                    </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                        Trong tuần này, kỹ năng nghe của bạn đạt mức khá. Tuy nhiên hãy tiếp tục trau dồi và luyện nghe nhiều hơn nữa vì thực sự kỹ năng nghe luôn được đánh là là một trong những phần khó nhất trong giao tiếp tiếng anh. Ngoài những bài học trên lớp, bạn có thể chủ động luyện nghe thông qua những bài tự học bổ ích được cập nhập hàng ngày của Topica. Nếu cần sự tư vấn về phương pháp học hay những khó khăn, bạn cứ chủ động liên hệ và chia sẻ với quản lý học tập. Hy vọng tuần tới mức điểm thi của bạn sẽ tốt hơn.
                                    </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                     Trong giao tiếp hằng ngày, người Mỹ, Anh thường dùng Cụm động từ/Thành ngữ để diễn tả ý tưởng, suy nghĩ, hành động, tiên đoán...của mình. Kết quả bài kiểm tra của bạn dù ở mức tương đối khá nhưng vẫn rất cần sự nỗ lực và cải thiện thêm. Hãy cùng Topica luyện tập và cải thiện những kiến thức về cụm động từ/thành ngữ hơn nữa nhé! 
                                    </td>
                                </tr>

                                <tr>
                                    <td bgcolor="#dbac69" style="color:#000;border: 1px solid black;width:20%;">
                                        Đúng 9 - 12: Khá
                                    </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                        Kết quả bài kiểm tra của bạn khá tốt. Bên cạnh những kiến thức bạn học được từ phía nhà trường mình nên mở rộng thêm từ các bài tập tự học trong học liệu quốc tế, thư viện mở rộng.... từ đó điểm số của bạn sẽ cao hơn trong bài kiểm tra tuần tới. Chúc may mắn!
                                    </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                        Kết quả bài kiển tra của bạn khá tốt. Hãy phát huy hơn nữa khả năng về ngữ pháp trong các tuần tiếp theo nhé ! Ngoài ra, hãy cố gắng mở rộng thêm từ các bài tập tự học trong học liệu quốc tế, thư viện mở rộng.... từ đó điểm số của bạn sẽ cao hơn trong bài kiểm tra tuần tới. Chúc may mắn!
                                    </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                        Kỹ năng nghe của bạn khá tốt. Bạn hãy cố gắng duy trì và cải thiện hơn nữa trong thời gian tới để sang tuần đạt kết quả cao hơn. Những bài học trên lớp là chưa đủ, hãy lên kế hoạch luyện tập những bài tự học mà quản lý học tập gửi hàng ngày để tăng phản xạ và bổ sung vốn từ vựng tiếng anh hơn nữa nhé!
                                    </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                    Những kiến thức về Cụm động từ/thành ngữ của bạn khá tốt. Bạn đã nắm được những nội dung cơ bản của các Cụm đồng từ/thành ngữ đã học trong tuần này. Tuy nhiên bạn vẫn cần luyện tập thêm để có thể sử dụng linh hoạt những cụm từ này trong các tình huống khác nhau. Hãy có gắng phát huy và học tập chăm chỉ hơn nữa nhé!
                                    </td>
                                </tr>

                                <tr>
                                    <td bgcolor="#dbac69" style="color:#000;border: 1px solid black;width:20%;">
                                        Đúng 13 - 15: Giỏi
                                    </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                        Thật là tuyệt vời, tuần này bạn đã học tập thật chăm chỉ và tiếp thu được lượng kiến thức trong tuần. Hãy tiếp tục duy trì tiến độ học tập tốt này của mình nhé!
                                    </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                        Chúc mừng bạn! Số điểm thi ngữ pháp của bạn thật đáng ghi nhận. Nắm chắc ngữ pháp là một trong những chiều khóa  để cuộc giao tiếp tiếng anh của bạn trở nên lôi cuốn và thu hút hơn rất nhiều. Hãy tiếp tục phát huy và trau dồi kiến thức nhiều hơn nữa trong các tuần tiếp theo nhé!
                                    </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                        Chúc mừng bạn với kết quả nghe thật đáng khen ngợi. Kỹ năng nghe là một trong những kỹ năng khó nhất trong giao tiếp tiếng anh nên hãy cố gắng trau dồi và luyện tập kỹ năng nghe hơn nữa nhé! Cố gắng giữ vững phong độ và thường xuyên luyện tập, chắc chắn giao tiếp tiếng anh thành thạo sẽ nằm trong tầm tay bạn.
                                    </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                    Vốn kiến thức về Cụm động từ/Thành ngữ của bạn tuần này rất tốt. Bạn đã nắm chắc được định nghĩa và cách sử dụng của các cụm từ đã học trong tuần. Chắc chắn trong thời gian qua bạn đã luyện tập và trau dồi rất nhiều. Hy vọng trong thời gian tới, trình độ cũng như kết quả học tập của bạn sẽ tiến bộ hơn nữa!
                                    </td>
                                </tr>
                                </tbody>
                            </table>';

$table_element_default = '                            <table style="width:100%; border-collapse: collapse;text-align:center;">
                                <tbody>
                                <tr style="height: 50px">
                                    <th bgcolor="#dbac69" style="color:#000;border: 1px solid black;width:20%; font-size: 14px;">
                                        Điểm thành phần
                                    </th>
                                    <th bgcolor="#dbac69" style="color:#000;border: 1px solid black;width:20%; font-size: 14px;">
                                        Sử dụng từ vựng
                                    </th>
                                    <th bgcolor="#dbac69" style="color:#000;border: 1px solid black;width:20%; font-size: 14px;">
                                        Sử dụng ngữ pháp
                                    </th>
                                    <th bgcolor="#dbac69" style="color:#000;border: 1px solid black;width:20%;font-size: 14px;">
                                        Nghe
                                    </th>
                                    <th bgcolor="#dbac69" style="color:#000;border: 1px solid black;width:20%; font-size: 14px;">
                                        Ngữ âm
                                    </th>
                                </tr>
                                <tr>
                                    <td bgcolor="#dbac69" style="color:#000;border: 1px solid black;width:20%;">
                                        Đúng 0 - 4: Kém
                                    </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                        Khả năng sử dụng từ vựng của bạn rất đáng lo ngại, bạn cần liên hệ với quản lý học tập để nhận được hỗ trợ tốt nhất. Ngoài ra bạn cần dành thời gian nhiều hơn cho việc học ở trên lớp và tự học trên kho học liệu của Topica, ngoài các bài học trên lớp nhé!
                                        </br><b>Tell me and I forget. Teach me and I remember. Involve me and I learn</b>
                                    </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                        Vốn ngữ pháp của bạn còn khá hạn chế, bạn cần trau dồi và luyện tập cải thiện ngữ pháp nhiều hơn nữa để có thể tự tin hơn khi giao tiếp tiếng anh. Nếu gặp khó khăn trong việc định hướng và tìm phương pháp học ngữ pháp tốt, hãy liên hệ với quản lý học tập để nhận hỗ trợ tốt nhất.
                                        </br><b>It doesn’t matter how slowly you go as long as you do not stop</b>
                                    </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                        Kỹ năng nghe của bạn thực sự đáng lo ngại. Bạn cần dành thời gian luyện nghe nhiều hơn nữa để giúp bản thân tự tin hơn khi giao tiếp tiếng anh. Sẽ thực sự khó trong giao tiếp khi mình không thể nghe được đối phương nói gì phải không nào? Đừng chần chờ gì nữa, hãy lên kế hoạch học tập cải thiện kỹ năng nghe ngay thôi.
                                        </br><b>Ever tried. Ever failed. No matter. Try again. Fail again. Fail better – Samuel Beckett</b>
                                    </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                        Kiến thức về ngữ âm của bạn thực sự cần cải thiện và trau dồi nhiều. Do vậy, hãy dành thời gian luyện và học ngữ âm nhiều hơn nữa để có thể giao tiếp tiếng anh một cách tự tin nhất! Hãy quyết tâm để không còn cảnh mình nói một kiểu và đối phương hiểu thành nghĩa khác bạn nhé.
                                        </br><b>Learning is like rowing upstream, not to advance is to drop back</b>
                                    </td>
                                </tr>

                                <tr>
                                    <td bgcolor="#dbac69" style="color:#000;border: 1px solid black;width:20%;">
                                        Đúng 5 - 8: Trung bình
                                    </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                        Bạn cần cố gắng hơn nữa trong việc học tập và ghi nhớ từ vựng. Kết quả bài kiểm tra của bạn còn ở mức khiêm tốn nên rất cần sự nỗ lực và quyết tâm hơn nữa trong việc học từ mới. Ngoài những kiến thức bài học từ phía nhà trường, bạn nên mở rộng thêm từ các bài tập tự học trong học liệu quốc tế, thư viện mở rộng.... để ghi nhớ từ vựng nhiều hơn nhé!
                                    </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                        Kết thúc tuần này, Ngữ pháp của bạn đã đạt mức điểm khá. Mặc dù vậy, bạn vẫn cần luyện tập và học thêm nhiều kiến thức về phần ngữ pháp để giúp bản thân có thể tự tin hơn nữa khi giao tiếp tiếng anh. Hãy liên hệ với quản lý học tập ngay nếu như cần hỗ trợ hay tư vấn về phương pháp học cũng như cách thức học ngữ pháp để đạt hiệu quả cao nhất nhé!
                                    </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                        Trong tuần này, kỹ năng nghe của bạn đạt mức khá. Tuy nhiên hãy tiếp tục trau dồi và luyện nghe nhiều hơn nữa vì thực sự kỹ năng nghe luôn được đánh là là một trong những phần khó nhất trong giao tiếp tiếng anh. Ngoài những bài học trên lớp, bạn có thể chủ động luyện nghe thông qua những bài tự học bổ ích được cập nhập hàng ngày của Topica. Nếu cần sự tư vấn về phương pháp học hay những khó khăn, bạn cứ chủ động liên hệ và chia sẻ với quản lý học tập. Hy vọng tuần tới mức điểm thi của bạn sẽ tốt hơn.
                                    </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                        Ngữ âm đóng vai trò quan trọng trong việc quyết định liệu mình có thể nói chuyện và giao tiếp tốt hay không? Kết quả bài kiểm tra dù ở mức khá nhưng vẫn rất cần sự nỗ lực và cải thiện nhiều.  Hãy cùng Topica luyện tập và cải thiện những kiến thức về ngữ âm hơn nữa nhé!
                                    </td>
                                </tr>

                                <tr>
                                    <td bgcolor="#dbac69" style="color:#000;border: 1px solid black;width:20%;">
                                        Đúng 9 - 12: Khá
                                    </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                        Kết quả bài kiểm tra của bạn khá tốt. Bên cạnh những kiến thức bạn học được từ phía nhà trường mình nên mở rộng thêm từ các bài tập tự học trong học liệu quốc tế, thư viện mở rộng.... từ đó điểm số của bạn sẽ cao hơn trong bài kiểm tra tuần tới. Chúc may mắn!
                                    </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                        Kết quả bài kiển tra của bạn khá tốt. Hãy phát huy hơn nữa khả năng về ngữ pháp trong các tuần tiếp theo nhé ! Ngoài ra, hãy cố gắng mở rộng thêm từ các bài tập tự học trong học liệu quốc tế, thư viện mở rộng.... từ đó điểm số của bạn sẽ cao hơn trong bài kiểm tra tuần tới. Chúc may mắn!
                                    </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                        Kỹ năng nghe của bạn khá tốt. Bạn hãy cố gắng duy trì và cải thiện hơn nữa trong thời gian tới để sang tuần đạt kết quả cao hơn. Những bài học trên lớp là chưa đủ, hãy lên kế hoạch luyện tập những bài tự học mà quản lý học tập gửi hàng ngày để tăng phản xạ và bổ sung vốn từ vựng tiếng anh hơn nữa nhé!
                                    </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                        Những kiến thức về ngữ âm của bạn khá tốt. Cõ lẽ bạn đã luyện tập trau dồi rất nhiều trong tuần vừa qua. Hãy có gắng phát huy và học tập chăm chỉ hơn nữa nhé!
                                    </td>
                                </tr>

                                <tr>
                                    <td bgcolor="#dbac69" style="color:#000;border: 1px solid black;width:20%;">
                                        Đúng 13 - 15: Giỏi
                                    </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                        Thật là tuyệt vời, tuần này bạn đã học tập thật chăm chỉ và tiếp thu được lượng kiến thức trong tuần. Hãy tiếp tục duy trì tiến độ học tập tốt này của mình nhé!
                                    </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                        Chúc mừng bạn! Số điểm thi ngữ pháp của bạn thật đáng ghi nhận. Nắm chắc ngữ pháp là một trong những chiều khóa  để cuộc giao tiếp tiếng anh của bạn trở nên lôi cuốn và thu hút hơn rất nhiều. Hãy tiếp tục phát huy và trau dồi kiến thức nhiều hơn nữa trong các tuần tiếp theo nhé!
                                    </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                        Chúc mừng bạn với kết quả nghe thật đáng khen ngợi. Kỹ năng nghe là một trong những kỹ năng khó nhất trong giao tiếp tiếng anh nên hãy cố gắng trau dồi và luyện tập kỹ năng nghe hơn nữa nhé! Cố gắng giữ vững phong độ và thường xuyên luyện tập, chắc chắn giao tiếp tiếng anh thành thạo sẽ nằm trong tầm tay bạn.
                                    </td>
                                    <td style="background-color:#fff;border: 1px solid black;width:20%; text-align: left;padding-left:5px;">
                                        Vốn kiến thức Ngữ âm của bạn tuần này thật tốt. Chắc chắn trong thời gian qua bạn đã luyện tập và trau dồi rất nhiều. Hy vọng trong thời gian tới, trình độ cũng như kết quả học tập của bạn sẽ tiến bộ hơn nữa!
                                    </td>
                                </tr>
                                </tbody>
                            </table>';

//title

$levelstudy = substr(strtolower($data->levelstudy), 0, 5);
switch ($levelstudy) {
    case "inter":
        $table_element = $table_element_inter;
        break;
    default:
        $table_element = $table_element_default;
        break;
}
//tungns4
if($data->quiz_name == "Speaking" || $data->quiz_name == "speaking"){
    $table_content = $table_content_speaking;
    $table_report = "
        <div id=\"container\" style=\" margin: 0 auto\"><br><br>
            $table_report_speaking
        </div>";
}else{
    $table_content = $table_content_default;
    $table_report = "<div style=\"width:40%;float:left;color:#fff;\">Mức độ thành thạo trên tổng thể</div>
        <div style=\"width:60%o; float: right;color:#fff;\">Mức độ thành thạo của bạn được đánh giá qua
            bảng điểm.
        </div>
        <div id=\"container\" style=\" margin: 0 auto\"><br><br>
            $table_consider
            <br><br>
            $table_element
        </div>";
}
//end tungns4
?>

<!DOCTYPE html>
<html dir="ltr" lang="en" xml:lang="en">
<head>
    <title></title>
    <link rel="shortcut icon"
          href="http://englishtest.topicanative.edu.vn/theme/image.php/boost/theme/1531559802/favicon"/>
    <link rel="stylesheet" type="text/css"
          href="http://tracnghiem.topicanative.edu.vn/theme/clean/style/tracnghiem.css">
    <script type="text/javascript"
            src="http://tracnghiem.topicanative.edu.vn/mod/quiz/js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="http://tracnghiem.topicanative.edu.vn/mod/quiz/js/jquery.plugin.js"></script>
    <script type="text/javascript" src="http://tracnghiem.topicanative.edu.vn/mod/quiz/js/disableBack.js"></script>

    <style>
        #container {
            width: auto;
            font-size: 11px;
        }

        #page {
            background-image: linear-gradient(rgba(255, 255, 255, 0.5), rgba(255, 255, 255, 0.5)), url(img/brgtn.jpeg) !important;
            position: absolute !important;
            width: 100% !important;
            height: 100% !important;
            top: 0 !important;
            left: 0 !important;
            overflow: auto !important;
            background-size: 100% 100% !important;
        }

        #hedder-tn {
            height: 50px;
            background-color: rgba(0, 0, 0, 0.8);
            width: 100%;

            position: absolute;

        }

        #hedder-tn .img-logo {
            height: 50px;
            box-sizing: border-box;
            padding: 10px;
            margin-left: 10%;
        }

        .content span {
            font-size: 16px !important;
        }

        #wrapper_container {
            margin-bottom: 0;
        }

        #welcome {
            width: 50%;
            padding: 10px;
            float: none;
            margin: 0 auto;
            margin-top: 130px;
            font-size: 13px;
            border-bottom: 0;
            box-sizing: border-box;
            background-color: rgba(0, 0, 0, 0.9);
            color: #dbac69;
        }

        #notification {
            margin-top: 0;
            background-color: rgba(0, 0, 0, 0.5) !important;
            color: #dbac69;
            width: 50%;
            margin: 0 auto;
        }

        #notification .note {
            color: #dbac69;
        }

        #wrapper_footer {
            display: none !important;
        }

        #wrapper_header {
            display: none !important;
        }
        /* tungns4 */
        .table_content_speaking{
            color:#000;
            border: 1px solid black;
            width:20%;
            font-size : 15px;
        }
        .overall{
            color:#000;
            border: 1px solid black;
            width:24%;
            font-size : 15px;
        }
        .name{
            color:#000;
            border: 1px solid black;
            width:  25%;
            font-size : 15px;
            text-align:center;
            height: 40px;
        }
        .report_part{
            color:#000;
            border: 1px solid black;
            width:12%;
            font-size : 13px;
        }
        .criteria {
            color:#000;
            border: 1px solid black;
            width:7%;
            font-size : 13px;
        }
        .description {
            color:#000;
            border: 1px solid black;
            width:17%;
            font-size : 13px;
        }
        .padding{
            padding: 5px;
            font-size:15px;
            text-align: justify;
        }
        .title{
            font-size:15px;
        }
        /* end tungns4 */
    </style>
</head>

<body>
<div id="page">
    <div id="wrapper_container">
        <div id="container">
            <div class="content">
                <span id="maincontent"></span>
                <link rel='stylesheet' href='http://fontawesome.io/assets/font-awesome/css/font-awesome.css'>
                <div id="container" style="width:90%; margin: 0 auto;margin-top:30px;">
                    <br><br>
                    <div style="text-align:center;">
                        <h1 style="color: #dbac69;font-weight: bolder;padding: 10px;background: rgba(0,0,0,0.8);box-sizing: border-box;text-align: center;margin-bottom:0;">
                            BÁO CÁO ĐIỂM</h1>
                    </div>
                    <div class="content-body"
                         style="overflow:auto;padding: 20px;box-sizing: border-box;margin-bottom:90px;;background: rgba(0,0,0,0.5);">
                        <div style="text-align:center">
                            <p style="color:#fff;">(Nội dung phản hồi về bài kiểm tra)</p>
                        </div>
                            <!-- tungns4 -->
                            <?php echo $table_content; ?>
                            <!-- end tungns4 -->
                        <br><br>
                        <?php echo $table_report; ?>
                    </div>
                </div>
            </div>
        </div><!--End Container-->
    </div><!--End wrapper_container-->
</div><!--End Page-->
</body>
</html>

