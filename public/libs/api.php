<?php
if (!function_exists('curl_version')) {
    exit ("Enable cURL in PHP");
}

function call_API($url, $data)
{

    $timeout = 0;
    $ch = curl_init($url);
    curl_setopt ( $ch, CURLOPT_URL, $url );
    curl_setopt ( $ch, CURLOPT_HEADER, 0 );
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
    curl_setopt ( $ch, CURLOPT_CONNECTTIMEOUT, $timeout );
    $curl_response = curl_exec($ch);
    if (curl_errno($ch)) {
        echo curl_error($ch);
        curl_close($ch);
        exit ();
    }
    curl_close($ch);
    $results = (array)json_decode($curl_response);
    return $results;

}


