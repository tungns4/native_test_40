<?php

global $CFG,$DB,$USER;

if(!isset($CFG)){
    require_once("../../config.php");
}
$flag       = optional_param('flag', "", PARAM_TEXT);
$attempt    = optional_param('attempt', "", PARAM_TEXT); 
$page       = optional_param('page', 0, PARAM_INT); 
if($flag == "getInfo"){
    $sql = 'SELECT * FROM top_question q JOIN top_question_attempts qta ON q.id = qta.questionid JOIN top_quiz_attempts qza ON qta.questionusageid = qza.uniqueid WHERE qza.id = '.$attempt.' AND qta.slot ='.$page;
    $cm = $DB->get_record_sql($sql, array());
    
    echo  json_encode($cm)  ;    
}
if($flag == "saveResponse"){
    $response = optional_param('response',"",PARAM_TEXT);
    $question_id = optional_param('question_id',0,PARAM_INT);
    if($response == "bạn nói không rõ tiếng, xin vui lòng nói lại"){
        echo "false";
    }else if($response == "grader_info thieu dau ','"){
        echo "false";
    }
    else{
        $pram = json_decode($response);
        $user_id = $USER->id;
        $sql = "SELECT * FROM top_result_ai WHERE question_id = $question_id and attempt_id = $attempt and user_id = $user_id";
        $check = $DB->get_record_sql($sql, array());
        if($check == false){
            $result_ai = new StdClass();
            $result_ai->question_id = $question_id;
            $result_ai->user_id = $USER->id;
            $result_ai->speech_result = $pram->speech_result;
            $result_ai->answer = $pram->answer;
            $result_ai->result = $pram->result;
            $result_ai->ratio = $pram->ratio;
            if($pram->right_word != ""){
                $result_ai->right_word = json_encode($pram->right_word);
            }else{
                $result_ai->right_word = "";
            }
            if($pram->wrong_word != ""){
                $result_ai->wrong_word = json_encode($pram->wrong_word);
            }else{
                $result_ai->wrong_word= "";
            }
            $result_ai->wpm = $pram->wpm;
            $result_ai->speed = $pram->speed;
            $result_ai->attempt_id = $attempt;
            if($pram->compare_text && $pram->ratio == 100){
                $result_ai->compare_text = $pram->compare_text;
            }
            if($pram->predicted_tense ){
                $result_ai->predicted_tense = $pram->predicted_tense;
            }
            if($pram->student_tense ){
                $result_ai->student_tense = $pram->student_tense;
            }
            if($pram->right_tense ){
                $result_ai->right_tense = $pram->right_tense;
            }
            $result_ai->id = $DB->insert_record('result_ai', $result_ai);
            echo $result_ai->id;
        } else{
            $data_update = new StdClass();
            $data_update->id = $check->id;
            $data_update->user_id = $USER->id;
            $data_update->speech_result = $pram->speech_result;
            $data_update->answer = $pram->answer;
            $data_update->result = $pram->result;
            $data_update->ratio = $pram->ratio;
            if($pram->right_word != ""){
                $data_update->right_word = json_encode($pram->right_word);
            }else{
                $data_update->right_word = "";
            }
            if($pram->wrong_word != ""){
                $data_update->wrong_word = json_encode($pram->wrong_word);
            }else{
                $data_update->wrong_word= "";
            }
            $data_update->wpm = $pram->wpm;
            $data_update->speed = $pram->speed;
            $data_update->attempt_id = $attempt;
            if($pram->compare_text && $pram->ratio == 100){
                $data_update->compare_text = $pram->compare_text;
            }else{
                $data_update->compare_text = "";
            }
            if($pram->predicted_tense ){
                $data_update->predicted_tense = $pram->predicted_tense;
            }
            if($pram->student_tense ){
                $data_update->student_tense = $pram->student_tense;
            }
            if($pram->right_tense ){
                $data_update->right_tense = $pram->right_tense;
            }
            $DB->update_record('result_ai', $data_update, $bulk=false);
            echo $data_update->id;
        }
        
    }
}
function insert_grades($attemptid)
{
    global $CFG,$DB,$USER;
    $grade = 0;
    $question_list = $DB->get_records_sql('SELECT q.id, q.name ,qza.quiz FROM  top_question q INNER JOIN top_question_attempts qta ON q.id = qta.questionid  INNER JOIN top_quiz_attempts qza ON qta.questionusageid = qza.uniqueid WHERE  qza.id = '.$attemptid.' AND ( q.name LIKE "%P1%" OR q.name LIKE "%P2%" OR q.name LIKE "%P3%" OR q.name LIKE "%P4%" ) GROUP BY q.id', array());
    
    foreach($question_list as $key => $question){ 
        $quiz =$question->quiz;
        $result_ai = $DB->get_record_sql('SELECT * FROM top_result_ai WHERE question_id = '.$question->id.' and attempt_id ='.$attemptid.' group by id desc', array());

        if($result_ai){
            switch (substr($question->name,0,2)) {
                case "P1":
                    $grade += $result_ai->ratio;
                    break;
                case "P2":
                    $grade += ($result_ai->ratio/100);
                    break;
                case "P3":
                    $grade += ($result_ai->ratio/100);
                    break;
                case "P4":
                    $grade += ($result_ai->ratio/100);
                    break;
                default:
            }
        }
    }

    $info = new StdClass();
    $quiz_grades->quiz = $quiz;
    $quiz_grades->userid = $USER->id;
    $quiz_grades->grade = $grade;
    $quiz_grades->timemodified =  time();
    $quiz_grades->checkrequest = 0;
    $quiz_grades->id = $DB->insert_record('quiz_grades', $quiz_grades);

    $data_update = new StdClass();
    $data_update->id = $attemptid;
    $data_update->sumgrades = $grade;
    $DB->update_record('quiz_attempts', $data_update, $bulk=false);

    return $quiz_grades->id;

}