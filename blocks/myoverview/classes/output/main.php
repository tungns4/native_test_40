<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Class containing data for my overview block.
 *
 * @package    block_myoverview
 * @copyright  2017 Ryan Wyllie <ryan@moodle.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace block_myoverview\output;
defined('MOODLE_INTERNAL') || die();

use renderable;
use renderer_base;
use templatable;
use core_completion\progress;

require_once($CFG->dirroot . '/blocks/myoverview/lib.php');
require_once($CFG->libdir . '/completionlib.php');

/**
 * Class containing data for my overview block.
 *
 * @copyright  2017 Simey Lameze <simey@moodle.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class main implements renderable, templatable {

    /**
     * @var string The tab to display.
     */
    public $tab;

    /**
     * Constructor.
     *
     * @param string $tab The tab to display.
     */
    public function __construct($tab) {
        $this->tab = $tab;
    }

    /**
     * Export this data so it can be used as the context for a mustache template.
     *
     * @param \renderer_base $output
     * @return stdClass
     */
    public function export_for_template(renderer_base $output) {
        global $USER;

        $courses = enrol_get_my_courses('*', 'fullname ASC');
        $coursesprogress = [];

        foreach ($courses as $course) {

            $completion = new \completion_info($course);

            // First, let's make sure completion is enabled.
            if (!$completion->is_enabled()) {
                continue;
            }

            $percentage = progress::get_course_progress_percentage($course);
            if (!is_null($percentage)) {
                $percentage = floor($percentage);
            }

            $coursesprogress[$course->id]['completed'] = $completion->is_course_complete($USER->id);
            $coursesprogress[$course->id]['progress'] = $percentage;
        }

        $coursesview = new courses_view($courses, $coursesprogress);
        $nocoursesurl = $output->image_url('courses', 'block_myoverview')->out();
        $noeventsurl = $output->image_url('activities', 'block_myoverview')->out();

        $dashboardview = main::render_dashboard_view($output);
        // Now, set the tab we are going to be viewing.
        $viewingdashboard = false;
        $viewingtimeline = false;
        $viewingcourses = false;
        if ($this->tab == BLOCK_MYOVERVIEW_DASHBOARD_VIEW) {
            $viewingdashboard = true;
        } else if ($this->tab == BLOCK_MYOVERVIEW_TIMELINE_VIEW) {
            $viewingtimeline = true;
        } else {
            $viewingcourses = true;
        }

        return [
            'midnight' => usergetmidnight(time()),
            'coursesview' => $coursesview->export_for_template($output),
            'dashboardview' => $dashboardview,
            'urls' => [
                'nocourses' => $nocoursesurl,
                'noevents' => $noeventsurl
            ],
            'viewingdashboard' => $viewingdashboard,
            'viewingtimeline' => $viewingtimeline,
            'viewingcourses' => $viewingcourses
        ];
    }

    public function render_dashboard_view($OUTPUT) {
        global $DB, $USER;
        $date = strtotime('-7 day');
        $last_week = date('W', $date);
        $last_week = (strlen($last_week) == 1) ? "0".$last_week : $last_week;
        $last_month = date('m');
        $last_year = date('Y', $date);

        $level_course = array(
            "sbasic" => "SBASIC",
            "basic" => "BASIC",
            "basic100" => "BASIC",
            "basic200" => "BASIC",
            "basic300" => "BASIC",
            "inter100" => "INTER",
            "inter200" => "INTER",
            "inter300" => "INTER"
        );

        $is_admin = (is_siteadmin()) ? true : false;
        if($is_admin){
            $levels = array("sbasic", "basic", "basic100", "basic200", "basic300", "inter100", "inter200", "inter300");
            $level = ($_POST["levelstudy"])? $_POST["levelstudy"] : "sbasic";
        }else{
            $level = $USER->levelstudy;
        }

        // Lấy top học viên có điểm thi cao nhất tuần trước đó cùng trình độ
        $viewhtml = "<br><label>Top học viên trình độ <b>{$level}</b> điểm cao nhất tuần <b>{$last_week}</b> năm <b>{$last_year}</b></label>";
        if($is_admin){
            $viewhtml .= '<form action="" method="POST" style="display: flex; float: right">';
                $viewhtml .= '<div class="form-group">
                    <select name="levelstudy" class="form-control">';
                    foreach($levels as $value){
                        $selected = ($level == $value) ? 'selected' : '';
                        $viewhtml .= "<option value='{$value}' {$selected}>".strtoupper($value)."</option>";
                    }
                    $viewhtml .= '</select>
                </div>';
            $viewhtml .= '<div class="form-group">
                <button type="submit" class="btn btn-default" style="margin-left: 15px">Tìm kiếm</button>
            </div>
        </form>';
        }

        $top_grades = $DB->get_records_sql("SELECT qg.id AS QGID, u.email, u.firstname, u.lastname, c.shortname, (SUM(qg.grade)*10) as grade_medium
            FROM {quiz_grades} as qg
            INNER JOIN {user} as u ON qg.userid = u.id
            INNER JOIN {quiz} as q ON qg.quiz = q.id
            INNER JOIN {course} as c ON q.course = c.id
            WHERE c.shortname = ? AND c.visible = ? AND u.levelstudy = ?
            GROUP BY c.id, u.email HAVING COUNT(qg.id) = ?
            ORDER BY grade_medium DESC", array("{$level_course[$level]}-W{$last_week}-{$last_year}", 1, $level, 5), 0, 15);

        $slots = $DB->get_record_sql("SELECT COUNT(qs.id) as number_slots
            FROM {quiz} as q
            INNER JOIN {quiz_slots} as qs ON qs.quizid = q.id
            INNER JOIN {course} as c ON q.course = c.id
            WHERE c.shortname = ? AND c.visible = ?", array("{$level_course[$level]}-W{$last_week}-{$last_year}", 1));

        $viewhtml .= '<table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Email</th>
                                    <th scope="col">Họ và tên</th>
                                    <th scope="col">Điểm thi trung bình</th>
                                </tr>
                            </thead>
                            <tbody>';
        foreach($top_grades as $top_grade){
            $viewhtml .= "<tr>
                <td>{$top_grade->email}</td>
                <td>{$top_grade->lastname} {$top_grade->firstname}</td>
                <td>".number_format($top_grade->grade_medium/$slots->number_slots, 3)."</td>
            </tr>";
        }
        $viewhtml .= '</tbody></table>';

        // Lấy top học viên có điểm thi cao nhất tháng cùng trình độ
        $last_year_by_month = date('Y');
        $viewhtml .= "<br><label>Top học viên trình độ <b>{$level}</b> điểm cao nhất tháng <b>{$last_month}</b> năm <b>{$last_year_by_month}</b></label>";

        $start_month = "{$last_year_by_month}-{$last_month}-01";
        $start_week_by_month = date('W', strtotime($start_month));
        $end_month = date('Y-m-d', strtotime('-1 second', strtotime('+1 month', strtotime($start_month))));
        $end_week_by_month = date('W', strtotime($end_month));

        $course_arr = array();
        for($i = intval($start_week_by_month); $i <= intval($end_week_by_month); $i++){
            $week_temp = (strlen($i) == 1) ? "0".$i : $i;
            array_push($course_arr, "{$level_course[$level]}-W{$week_temp}-{$last_year_by_month}");
        }
        list($insql, $inparams) = $DB->get_in_or_equal($course_arr);
        $top_grades_month = $DB->get_records_sql("SELECT qg.id AS QGID, u.email, u.firstname, u.lastname, c.id, c.shortname, (SUM(qg.grade)*10) as grade_medium
            FROM {quiz_grades} as qg
            INNER JOIN {user} as u ON qg.userid = u.id
            INNER JOIN {quiz} as q ON qg.quiz = q.id
            INNER JOIN {course} as c ON q.course = c.id
            WHERE c.shortname {$insql} AND c.visible = ? AND u.levelstudy = ?
            GROUP BY c.id, u.email HAVING COUNT(qg.id) = ?
            ORDER BY grade_medium DESC", array_merge($inparams, array(1, $level, 5)), 0, 15);

        $slots_month = $DB->get_records_sql("SELECT c.id, COUNT(qs.id) as number_slots
            FROM {quiz} as q
            INNER JOIN {quiz_slots} as qs ON qs.quizid = q.id
            INNER JOIN {course} as c ON q.course = c.id
            WHERE c.shortname {$insql} AND c.visible = ?
            GROUP BY c.id", array_merge($inparams, array(1)));

        $viewhtml .= '<table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Email</th>
                                    <th scope="col">Họ và tên</th>
                                    <th scope="col">Tuần</th>
                                    <th scope="col">Điểm thi trung bình</th>
                                </tr>
                            </thead>
                            <tbody>';
        foreach($top_grades_month as $top_grade){
            $viewhtml .= "<tr>
                <td>{$top_grade->email}</td>
                <td>{$top_grade->lastname} {$top_grade->firstname}</td>
                <td>".str_replace("W", "", explode("-", $top_grade->shortname)[1])."</td>
                <td>".number_format($top_grade->grade_medium/$slots_month[$top_grade->id]->number_slots, 3)."</td>
            </tr>";
        }
        $viewhtml .= '</tbody></table>';

        // Lấy history học viên
        $viewhtml .= "<br><label>Lịch sử làm bài gần nhất</label>";

        $recent_quiz = $DB->get_record_sql("SELECT quiz FROM {quiz_attempts} WHERE userid = ? AND state = 'finished' ORDER BY timefinish DESC", array($USER->id));
        $recent_exams = $DB->get_records_sql("SELECT qa.id, c.shortname, quiz.name, qa.slot, qs.questionid, q.questiontext, q.generalfeedback, qa.rightanswer, qa.responsesummary, qs.maxmark
            FROM  top_quiz_slots AS qs
            INNER JOIN top_question_attempts AS qa ON (qa.questionusageid = qs.questionid AND qa.slot = qs.slot)
            INNER JOIN top_question AS q ON q.id = qa.questionid
            INNER JOIN top_quiz AS quiz ON quiz.id = qs.quizid
            INNER JOIN top_course AS c ON c.id = quiz.course
            WHERE q.length > 0 AND qs.quizid = ?
            ORDER BY qs.quizid, qa.slot DESC", array($recent_quiz->quiz));
        $viewhtml .= '<table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Course</th>
                                <th scope="col">Quiz</th>
                                <th scope="col">Question</th>
                                <th scope="col">GeneralFeedback</th>
                                <th scope="col">Right Answer</th>
                                <th scope="col">Response</th>
                                <th scope="col">Max Mark</th>
                                <th scope="col">Goal</th>
                            </tr>
                        </thead>
                        <tbody>';
        foreach($recent_exams as $exam){
            $viewhtml .= "<tr>
                <td>{$exam->shortname}</td>
                <td>{$exam->name}</td>
                <td style='max-width: 400px'>{$exam->questiontext}</td>
                <td style='max-width: 400px'>{$exam->generalfeedback}</td>
                <td style='max-width: 150px'>{$exam->rightanswer}</td>
                <td style='max-width: 150px'>{$exam->responsesummary}</td>
                <td>".number_format($exam->maxmark, 2)."</td>
                <td>".(($exam->responsesummary == $exam->rightanswer) ? '<i style="color: green" class="fa fa-check" aria-hidden="true"></i>' : '<i style="color: red" class="fa fa-close" aria-hidden="true"></i>')."</td>
            </tr>";
        }
        $viewhtml .= '</tbody></table>';

        //Biểu đồ
        $viewhtml .= '<form action="" method="POST" style="display: flex; float: right">';
            if($is_admin){
                $viewhtml .= '<div class="form-group">
                    <select name="levelstudy" class="form-control">';
                    foreach($levels as $value){
                        $selected = ($level == $value) ? 'selected' : '';
                        $viewhtml .= "<option value='{$value}' {$selected}>".strtoupper($value)."</option>";
                    }
                    $viewhtml .= '</select>
                </div>';
            }
                $viewhtml .= '<div class="form-group">
                    <select name="chartview" class="form-control">
                        <option value="week" '.(($_POST['chartview'] == 'month') ? '' : 'selected').'>WEEK</option>
                        <option value="month" '.(($_POST['chartview'] == 'month') ? 'selected': '').'>MONTH</option>
                    </select>
                </div>';
            $viewhtml .= '<div class="form-group">
                <button type="submit" class="btn btn-default" style="margin-left: 15px">Tìm kiếm</button>
            </div>
        </form>';
        $sales = new \core\chart_series('Sales', [1000, 1170, 660, 1030 ]);
        $expenses = new \core\chart_series('Expenses', [400, 460, 1120, 540]);
        $labels = ['2004', '2005', '2006', '2007'];

        // $chart = new \core\chart_pie();
        // $chart->set_title('PIE CHART');
        // $chart->add_series($sales);
        // $chart->set_labels($labels);

        // $chart2 = new \core\chart_pie();
        // $chart2->set_title('DOUGHNUT CHART');
        // $chart2->set_doughnut(true);
        // $chart2->add_series($sales);
        // $chart2->set_labels($labels);

        // $chart3 = new \core\chart_line();
        // $chart3->set_title('TENSIONED LINES CHART');
        // $chart3->add_series($sales);
        // $chart3->add_series($expenses);
        // $chart3->set_labels($labels);

        $chart = new \core\chart_bar();
        $chart->set_title('BAR CHART COMBINED WITH LINE CHART');
        $expenses->set_type(\core\chart_series::TYPE_LINE);
        $chart->add_series($expenses);
        $chart->add_series($sales);
        $chart->set_labels($labels);

        $viewhtml .= $OUTPUT->render($chart);

        return $viewhtml;
    }
}
