[![Build Status](https://travis-ci.org/EduardoKrausME/moodle-theme_degrade.svg?branch=master)](https://travis-ci.org/EduardoKrausME/moodle-theme_degrade)

## instalando

### Português

[Veja na Wiki o manual de instalação](https://github.com/EduardoKrausME/moodle-theme_degrade/wiki/BR-Instalando)

### English
[See the installation guide on the Wiki](https://github.com/EduardoKrausME/moodle-theme_degrade/wiki/EN-Installing)

## prints

![theme-01.png](https://raw.githubusercontent.com/EduardoKrausME/moodle-theme_degrade/master/pix/prints/theme-01.png)

![theme-02.png](https://raw.githubusercontent.com/EduardoKrausME/moodle-theme_degrade/master/pix/prints/theme-02.png)

![theme-03.png](https://raw.githubusercontent.com/EduardoKrausME/moodle-theme_degrade/master/pix/prints/theme-03.png)

![theme-04.png](https://raw.githubusercontent.com/EduardoKrausME/moodle-theme_degrade/master/pix/prints/theme-04.png)

![theme-05.png](https://raw.githubusercontent.com/EduardoKrausME/moodle-theme_degrade/master/pix/prints/theme-05.png)

![theme-06.png](https://raw.githubusercontent.com/EduardoKrausME/moodle-theme_degrade/master/pix/prints/theme-06.png)

![theme-07.png](https://raw.githubusercontent.com/EduardoKrausME/moodle-theme_degrade/master/pix/prints/theme-07.png)

![theme-08.png](https://raw.githubusercontent.com/EduardoKrausME/moodle-theme_degrade/master/pix/prints/theme-08.png)

![theme-09.png](https://raw.githubusercontent.com/EduardoKrausME/moodle-theme_degrade/master/pix/prints/theme-09.png)

![theme-10.png](https://raw.githubusercontent.com/EduardoKrausME/moodle-theme_degrade/master/pix/prints/theme-10.png)

 