<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A two column layout for the boost theme.
 *
 * @package   theme_boost
 * @copyright 2016 Damyon Wiese
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

user_preference_allow_ajax_update('drawer-open-nav', PARAM_ALPHA);
require_once($CFG->libdir . '/behat/lib.php');

$is_admin = false;
if (isloggedin()) {
    $navdraweropen = (get_user_preferences('drawer-open-nav', 'true') == 'true');
    $menuright = array(
        array(
            'action' => new Moodle_url('/user/profile.php?id='.$USER->id),
            'name' => get_string('profile'),
            'icon' => 'fa fa-user'
        ),
        array(
            'action' => new Moodle_url('/login/logout.php?sesskey='.sesskey()),
            'name' => get_string('logout'),
            'icon' => 'fa fa-sign-out'
        )
    );
    $is_admin = (is_siteadmin()) ? true : false;
    $user_picture = new user_picture($USER);
    $src = $user_picture->get_url($PAGE);
    $userinfo = array(
        'fullname' => fullname($USER),
        'avatar' => $src
    );
} else {
    if(isset($_SERVER['SCRIPT_NAME']) && strstr(trim($_SERVER['SCRIPT_NAME'], '/'), '/') == '/forgot_password.php'){
        $navdraweropen = false;
    }else{
        redirect(new moodle_url('/login/index.php'));
    }
}

$extraclasses = [];
if ($navdraweropen && $is_admin) {
    $extraclasses[] = 'drawer-open-left';
}
$bodyattributes = $OUTPUT->body_attributes($extraclasses);
$blockshtml = $OUTPUT->blocks('side-pre');
$hasblocks = strpos($blockshtml, 'data-block=') !== false;
$regionmainsettingsmenu = $OUTPUT->region_main_settings_menu();
$templatecontext = [
    'sitename' => format_string($SITE->shortname, true, ['context' => context_course::instance(SITEID), "escape" => false]),
    'output' => $OUTPUT,
    'sidepreblocks' => $blockshtml,
    'hasblocks' => $hasblocks,
    'bodyattributes' => $bodyattributes,
    'navdraweropen' => $navdraweropen,
    'regionmainsettingsmenu' => $regionmainsettingsmenu,
    'hasregionmainsettingsmenu' => !empty($regionmainsettingsmenu)
];

if(isloggedin()){
    $templatecontext['is_admin'] = $is_admin;
    $templatecontext['userinfo'] = $userinfo;
    $templatecontext['menuright'] = $menuright;
    if($is_admin){
        $templatecontext['flatnavigation'] = $PAGE->flatnav;
    }
}
echo $OUTPUT->render_from_template('theme_boost/columns2', $templatecontext);

