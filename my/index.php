<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * My Moodle -- a user's personal dashboard
 *
 * - each user can currently have their own page (cloned from system and then customised)
 * - only the user can see their own dashboard
 * - users can add any blocks they want
 * - the administrators can define a default site dashboard for users who have
 *   not created their own dashboard
 *
 * This script implements the user's view of the dashboard, and allows editing
 * of the dashboard.
 *
 * @package    moodlecore
 * @subpackage my
 * @copyright  2010 Remote-Learner.net
 * @author     Hubert Chathi <hubert@remote-learner.net>
 * @author     Olav Jordan <olav.jordan@remote-learner.net>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(__DIR__ . '/../config.php');
require_once('lib.php');

redirect_if_major_upgrade_required();

// TODO Add sesskey check to edit
$edit = optional_param('edit', null, PARAM_BOOL);    // Turn editing on and off
$reset = optional_param('reset', null, PARAM_BOOL);

require_login();

$hassiteconfig = has_capability('moodle/site:config', context_system::instance());
if ($hassiteconfig && moodle_needs_upgrading()) {
    redirect(new moodle_url('/admin/index.php'));
}

$strmymoodle = get_string('myhome');

if (isguestuser()) {  // Force them to see system default, no editing allowed
    // If guests are not allowed my moodle, send them to front page.
    if (empty($CFG->allowguestmymoodle)) {
        redirect(new moodle_url('/', array('redirect' => 0)));
    }

    $userid = null;
    $USER->editing = $edit = 0;  // Just in case
    $context = context_system::instance();
    $PAGE->set_blocks_editing_capability('moodle/my:configsyspages');  // unlikely :)
    $header = "$SITE->shortname: $strmymoodle (GUEST)";
    $pagetitle = $header;

} else {        // We are trying to view or edit our own My Moodle page
    $userid = $USER->id;  // Owner of the page
    $context = context_user::instance($USER->id);
    $PAGE->set_blocks_editing_capability('moodle/my:manageblocks');
    $header = fullname($USER);
    $pagetitle = $strmymoodle;
}

// Get the My Moodle page info.  Should always return something unless the database is broken.
if (!$currentpage = my_get_page($userid, MY_PAGE_PRIVATE)) {
    print_error('mymoodlesetup');
}

// Start setting up the page
$params = array();
$PAGE->set_context($context);
$PAGE->set_url('/my/?lang=vi', $params);
//$PAGE->set_pagelayout('mydashboard');
$PAGE->set_pagetype('my-index');

$PAGE->blocks->add_region('content');
$PAGE->set_subpage($currentpage->id);
$PAGE->set_title($pagetitle);
$PAGE->set_heading($header);

if (!isguestuser()) {   // Skip default home page for guests
    if (get_home_page() != HOMEPAGE_MY) {
        if (optional_param('setdefaulthome', false, PARAM_BOOL)) {
            set_user_preference('user_home_page_preference', HOMEPAGE_MY);
        } else if (!empty($CFG->defaulthomepage) && $CFG->defaulthomepage == HOMEPAGE_USER) {
            $frontpagenode = $PAGE->settingsnav->add(get_string('frontpagesettings'), null, navigation_node::TYPE_SETTING, null);
            $frontpagenode->force_open();
            $frontpagenode->add(get_string('makethismyhome'), new moodle_url('/my/', array('setdefaulthome' => true)),
                navigation_node::TYPE_SETTING);
        }
    }
}

// Toggle the editing state and switches
if (empty($CFG->forcedefaultmymoodle) && $PAGE->user_allowed_editing()) {
    if ($reset !== null) {
        if (!is_null($userid)) {
            require_sesskey();
            if (!$currentpage = my_reset_page($userid, MY_PAGE_PRIVATE)) {
                print_error('reseterror', 'my');
            }
            redirect(new moodle_url('/my'));
        }
    } else if ($edit !== null) {             // Editing state was specified
        $USER->editing = $edit;       // Change editing state
    } else {                          // Editing state is in session
        if ($currentpage->userid) {   // It's a page we can edit, so load from session
            if (!empty($USER->editing)) {
                $edit = 1;
            } else {
                $edit = 0;
            }
        } else {
            // For the page to display properly with the user context header the page blocks need to
            // be copied over to the user context.
            if (!$currentpage = my_copy_page($USER->id, MY_PAGE_PRIVATE)) {
                print_error('mymoodlesetup');
            }
            $context = context_user::instance($USER->id);
            $PAGE->set_context($context);
            $PAGE->set_subpage($currentpage->id);
            // It's a system page and they are not allowed to edit system pages
            $USER->editing = $edit = 0;          // Disable editing completely, just to be safe
        }
    }

    // Add button for editing page
    $params = array('edit' => !$edit);

    $resetbutton = '';
    $resetstring = get_string('resetpage', 'my');
    $reseturl = new moodle_url("$CFG->wwwroot/my/index.php", array('edit' => 1, 'reset' => 1));

    if (!$currentpage->userid) {
        // viewing a system page -- let the user customise it
        $editstring = get_string('updatemymoodleon');
        $params['edit'] = 1;
    } else if (empty($edit)) {
        $editstring = get_string('updatemymoodleon');
    } else {
        $editstring = get_string('updatemymoodleoff');
        $resetbutton = $OUTPUT->single_button($reseturl, $resetstring);
    }

    $url = new moodle_url("$CFG->wwwroot/my/index.php", $params);
    $button = $OUTPUT->single_button($url, $editstring);
    $PAGE->set_button($resetbutton . $button);

} else {
    $USER->editing = $edit = 0;
}

echo $OUTPUT->header();


//nvt change popup lang domain.com/my/
$uri = $_SERVER['REQUEST_URI'];
$level_study = substr(strtolower($USER->levelstudy), 0, 5);
$button_test_basic = '<div class="row" id="floor2">
                        <div class="col-xs-8 offset-xs-2 col-md-4 offset-md-4">
                            <button id="topica_choose_test_vocabulary" type="button" class="nut btn-popup">Vocabulary
                            </button>
                        </div>
                        <div class="col-xs-8 offset-xs-2 col-md-4 offset-md-4">
                            <button id="topica_choose_test_speaking" type="button" class="nut btn-popup">Speaking
                            </button>
                        </div>
                        <div class="col-xs-8 offset-xs-2 col-md-4 offset-md-4">
                            <button id="topica_choose_test_grammar" type="button" class="nut btn-popup">Grammar</button>
                        </div>
                        <div class="col-xs-8 offset-xs-2 col-md-4 offset-md-4">
                            <button id="topica_choose_test_listen" type="button" class="nut btn-popup">Listening
                            </button>
                        </div>
                        <div class="col-xs-8 offset-xs-2 col-md-4 offset-md-4">
                            <button id="topica_choose_test_pronunciation" type="button" class="nut btn-popup">
                                Pronunciation
                            </button>
                        </div>
                        <div class="col-xs-8 offset-xs-2 col-md-4 offset-md-4">
                            <button id="topica_choose_test_all" type="button" class="nut btn-popup">General</button>
                        </div>
                    </div>';

$button_test_inter = '<div class="row" id="floor2">
                        <div class="col-xs-8 offset-xs-2 col-md-4 offset-md-4">
                            <button id="topica_choose_test_vocabulary" type="button" class="nut btn-popup">Vocabulary
                            </button>
                        </div>
                        <div class="col-xs-8 offset-xs-2 col-md-4 offset-md-4">
                            <button id="topica_choose_test_speaking" type="button" class="nut btn-popup">Speaking
                            </button>
                        </div>
                        <div class="col-xs-8 offset-xs-2 col-md-4 offset-md-4">
                            <button id="topica_choose_test_grammar" type="button" class="nut btn-popup">Grammar</button>
                        </div>
                        <div class="col-xs-8 offset-xs-2 col-md-4 offset-md-4">
                            <button id="topica_choose_test_listen" type="button" class="nut btn-popup">Listening
                            </button>
                        </div>
                        <div class="col-xs-8 offset-xs-2 col-md-4 offset-md-4">
                            <button id="topica_choose_test_phrasal" type="button" class="nut btn-popup">
                                Phrasal verb/idiom
                            </button>
                        </div>
                        <div class="col-xs-8 offset-xs-2 col-md-4 offset-md-4">
                            <button id="topica_choose_test_all" type="button" class="nut btn-popup">General</button>
                        </div>
                    </div>';

$button_test_superbasic = '<div class="row" id="floor2">
                        <div class="col-xs-8 offset-xs-2 col-md-4 offset-md-4">
                            <button id="topica_choose_test_vocabulary" type="button" class="nut btn-popup">Vocabulary
                            </button>
                        </div>
                        <div class="col-xs-8 offset-xs-2 col-md-4 offset-md-4">
                            <button id="topica_choose_test_speaking" type="button" class="nut btn-popup">Speaking
                            </button>
                        </div>
                        <div class="col-xs-8 offset-xs-2 col-md-4 offset-md-4">
                            <button id="topica_choose_test_grammar" type="button" class="nut btn-popup">Grammar</button>
                        </div>
                        <div class="col-xs-8 offset-xs-2 col-md-4 offset-md-4">
                            <button id="topica_choose_test_listen" type="button" class="nut btn-popup">Listening
                            </button>
                        </div>
                        <div class="col-xs-8 offset-xs-2 col-md-4 offset-md-4">
                            <button id="topica_choose_test_pronunciation" type="button" class="nut btn-popup">
                                Pronunciation
                            </button>
                        </div>
                        <div class="col-xs-8 offset-xs-2 col-md-4 offset-md-4">
                            <button id="topica_choose_test_all" type="button" class="nut btn-popup">General</button>
                        </div>
                    </div>';

$landing_page = 'https://gioithieunguoithan.topica.vn/lms/?utm_source=nativetest&utm_medium=huyvq2-&utm_campaign=&utm_term=l8active&utm_content=';
if (strpos($uri, 'en')) {
    $welcome = 'Welcome';
    $welcome1 = "Welcome you to TOPICA Native's English Test";
    $welcome2 = 'Keep calm and pass the exam';
    $welcome3 = 'Click Start attempt to start exam or click Log out';
    $attempt = "Month's exam";
    $attempt_week = "Week's exam";
    $logout = 'Log out';
    $logout_url = $CFG->wwwroot . '/login/logout.php?sesskey=' . sesskey();
    $noty = 'Noty';
    $noty1 = 'You are not allow to the exam';
    $noty2 = 'Please contact to your advisor';
    $noty3 = 'to register the exam';
    $view_button = 'Go exam';
} else {
    $welcome = 'Chào mừng bạn đến với TOPICA Native';
    $welcome1 = 'TOPICA Native chào mừng anh/chị tham gia bài kiểm tra trình độ Tiếng Anh.';
    $welcome2 = 'Chúc anh/chị bình tĩnh và đạt kết quả tốt nhất trong bài thi này!';
    $welcome3 = 'Anh/chị vui lòng chọn làm bài để vào bài thi hoặc đăng xuất để thoát ra.';
    $attempt = 'Thi tháng';
    $logout = 'Đăng xuất';
    $logout_url = $CFG->wwwroot . '/login/logout.php?sesskey=' . sesskey();
    $attempt_week = 'Thi tuần';
    $noty = 'Thông báo';
    $noty1 = 'Bạn chưa được đăng ký bài thi này.';
    $noty2 = 'Vui lòng liên hệ với quản lý học tập của mình';
    $noty3 = 'để được đăng kí vào bài thi';
    $view_button = 'Vào bài thi';
}

$view_html .= '
    <style>
        .btn-open-popup {
            margin-top:10px;
            border: none;
            padding: 10px 20px;
            background-color : #dbac69;
            border-radius : 5px;
            width : 100%;font-weight : bold;
            font-size : 15px;
        }
        .btn-popup {
            margin-top:10px;
            border: none;
            padding: 7px 20px;
            background-color : #dbac69;
            border-radius : 5px;
            width : 100%;font-weight : bold;
            font-size : 15px;
        }
        .btn-popup-logout{
            border:none;
            padding: 7px 20px;
            background-color : #ddd;
            margin-top : 10px;
            background-color : #adabab;
            border-radius : 5px; 
            width : 100%; 
            font-weight : bold; 
            font-size : 15px;
            margin-left: 13em;
        }
        
    </style>
        <script type="text/javascript">
        $(document).ready(function(){
            setTimeout(function () {
                $("#topica_welcome_popup").click();
                $("#floor2").hide();
                $("#popup-back").hide();
            }, 300);
        });
        $("#topica_welcome_btn_start").click(function(){
            window.location.replace("' . $CFG->wwwroot . "/mod/quiz/view.php?id=" . get_cmid_month() . '");
        });
        $("#topica_choose_test_week").click(function(){
            $("#floor1").hide();
            $("#floor2").show();
            $("#popup-back").show();                
        });
        $("#popup-back").click(function(){
            $("#floor1").show();
            $("#floor2").hide();
            $("#popup-back").hide();                
        });
        $("#topica_choose_test_vocabulary").click(function(){
            if(' . get_cmid("vocabulary") . ' != null){
                window.location.replace("' . $CFG->wwwroot . "/mod/quiz/view.php?id=" . get_cmid("vocabulary") . '");
            }else{
                $("#topica_welcome_popup").click();
                $("#topica-msg").click();  
            }
        });
        $("#topica_choose_test_speaking").click(function(){
            if(' . get_cmid("speaking") . ' != null){
                window.location.replace("' . $CFG->wwwroot . "/mod/quiz/view.php?id=" . get_cmid("speaking")  . '");
            }else{
                $("#topica_welcome_popup").click();
                $("#topica-msg").click(); 
            }
        });
        $("#topica_choose_test_grammar").click(function(){
            if(' . get_cmid("grammar") . ' != null){
                window.location.replace("' . $CFG->wwwroot . "/mod/quiz/view.php?id=" . get_cmid("grammar") . '");
            }else{
                $("#topica_welcome_popup").click();
                $("#topica-msg").click(); 
            }
        });
        $("#topica_choose_test_listen").click(function(){
            if(' . get_cmid("listen") . ' != null){
                window.location.replace("' . $CFG->wwwroot . "/mod/quiz/view.php?id=" . get_cmid("listen") . '");
            }else{
                $("#topica_welcome_popup").click();
                $("#topica-msg").click(); 
            }
        });
        $("#topica_choose_test_pronunciation").click(function(){
            if(' . get_cmid("pronunciation") . ' != null){
                window.location.replace("' . $CFG->wwwroot . "/mod/quiz/view.php?id=" . get_cmid("pronunciation") . '");
            }else{
                $("#topica_welcome_popup").click();
                $("#topica-msg").click(); 
            }
        });
        $("#topica_choose_test_phrasal").click(function(){
            if(' . get_cmid("phrasal") . ' != null){
                window.location.replace("' . $CFG->wwwroot . "/mod/quiz/view.php?id=" . get_cmid("phrasal") . '");
            }else{
                $("#topica_welcome_popup").click();
                $("#topica-msg").click(); 
            }
        });
        $("#topica_choose_test_reading").click(function(){
            if(' . get_cmid("reading") . ' != null){
                window.location.replace("' . $CFG->wwwroot . "/mod/quiz/view.php?id=" . get_cmid("reading") . '");
            }else{
                $("#topica_welcome_popup").click();
                $("#topica-msg").click(); 
            }
        });
        $("#topica_choose_test_all").click(function(){
            if(' . get_cmid("GENERAL") . ' != null){
                window.location.replace("' . $CFG->wwwroot . "/mod/quiz/view.php?id=" . get_cmid("GENERAL") . '");
            }else{
                $("#topica_welcome_popup").click();
                $("#topica-msg").click(); 
            }
        });
    </script>
    ';
?>
    <div class="row">
        <div class="col-xs-6     offset-xs-2 col-md-2 offset-md-8">
            <button type="button" class="btn btn-open-popup" data-toggle="modal" data-target="#myModal"
                    id="topica_welcome_popup"><?php echo $view_button; ?></button>

        </div>
        <a href="<?php echo $landing_page; ?>" target="_blank">
            <img src="../theme/boost/img/button_gift.png">
        </a>
    </div>
    <div class="modal fade" id="myModal" role="dialog" tabindex="-1">
        <div class="modal-dialog" style="width: 100%;font-family: Arial, Helvetica, sans-serif;margin-top : 100px;">
            <div class="modal-content"
                 style="background-color: rgba(0,0,0,.7);border-radius: 0;border:none;text-align : center;">
                <div class="modal-header" style="padding: 0;border: none;">
                    <div class="row" style="background-color: rgba(0,0,0,.7); margin-left: 0px; margin-right: 0px;">
                        <div class="col-md-11 col-xs-10">
                            <h3 class="modal-title"
                                style="text-align: left;font-size: 15px;color: #dbac69;padding: 7px;"><?php echo $welcome; ?></h3>
                        </div>
                        <div class="col-md-1 col-xs-1" id="popup-back">
                            <i class="fa fa-arrow-circle-left fa-2x" aria-hidden="true" style="color:#dbac69;"></i>
                        </div>
                    </div>
                </div>
                <div class="modal-body" style="color: #000;padding: 20px 20px;">
                    <h6 style="color :#dbac69;"><?php echo $welcome1; ?></h6>
                    <h6 style="color :#dbac69;"><?php echo $welcome2; ?></h6>
                    <h6 style="color :#dbac69;"><?php echo $welcome3; ?></h6>
                    <div class="row" id="floor1">
                        <div class="col-xs-8 offset-xs-2 col-md-4 offset-md-2">
                            <button id="topica_welcome_btn_start" type="button"
                                    class="nut btn-popup"><?php echo $attempt; ?></button>
                        </div>
                        <div class="col-xs-8 offset-xs-2 col-md-4 offset-md-0">
                            <button id="topica_choose_test_week" type="button"
                                    class="nut btn-popup"><?php echo $attempt_week; ?></button>
                        </div>
                        <div class="col-xs-8 offset-xs-2 col-md-4 offset-md-0">
                            <a href="<?php echo $logout_url; ?>"
                               style="margin: unset">
                                <button type="button" class="nut thoat btn-popup-logout"><?php echo $logout; ?></button>
                            </a>
                        </div>
                    </div>
                    <?php

                    switch ($level_study) {
                        case 'sbasi' :
                            echo $button_test_superbasic;
                            break;
                        case 'basic':
                            echo $button_test_basic;
                            break;
                        case 'inter':
                            echo $button_test_inter;
                            break;
                        default:
                            echo $button_test_basic;
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>

    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#modal-msg" style="display:none"
            id="topica-msg">Open Modal
    </button>
    <div class="modal fade" id="modal-msg" role="dialog" tabindex="-1">
        <div class="modal-dialog" style="width: 100%;font-family: Arial, Helvetica, sans-serif;margin-top : 100px;">
            <div class="modal-content"
                 style="background-color: rgba(0,0,0,1);border-radius: 0;border:none;text-align : center;">
                <div class="modal-header" style="padding: 0;border: none;">
                    <div class="row" style="background-color: rgba(0,0,0,1); margin-left: 0px; margin-right: 0px;">
                        <div class="col-md-11 col-xs-10">
                            <h3 class="modal-title"
                                style="text-align: left;font-size: 15px;color: #dbac69;padding: 7px;"><?php echo $noty; ?></h3>
                        </div>
                    </div>
                </div>
                <div class="modal-body" style="color: #000;padding: 20px 20px;">
                    <h6 style="color :#dbac69;"><?php echo $noty1; ?></h6>
                    <h6 style="color :#dbac69;"><?php echo $noty2; ?></h6>
                    <h6 style="color :#dbac69;"><?php echo $noty3; ?></h6>
                    <div class="row">
                        <div class="col-xs-8 offset-xs-2 col-md-4 offset-md-4">
                            <a href="<?php echo $logout_url; ?>">
                                <button type="button" class="nut thoat btn-popup-logout"><?php echo $logout; ?></button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
echo $OUTPUT->custom_block_region('content');
echo $view_html;

// nvt end edit
echo $OUTPUT->footer();

// Trigger dashboard has been viewed event.
$eventparams = array('context' => $context);
$event = \core\event\dashboard_viewed::create($eventparams);
$event->trigger();
